# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-12-12 09:38+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: sa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: /hdd/projects/rockwork/rockwork/qml/rockwork.qml:92
#: /hdd/projects/rockwork/rockwork/qml/pages/LoadingPage.qml:42
msgid "Loading..."
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/components/HealthInfoItem.qml:224
msgid "KM"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/components/InstalledAppDelegate.qml:38
#: /hdd/projects/rockwork/rockwork/qml/pages/AppUpgradePage.qml:77
msgid "Upgrade"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/components/InstalledAppDelegate.qml:38
#: /hdd/projects/rockwork/rockwork/qml/pages/AppStoreDetailsPage.qml:272
#: /hdd/projects/rockwork/rockwork/qml/pages/AppUpgradePage.qml:49
msgid "Version"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/AlertDialog.qml:5
msgid "App Settings Alert"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/AlertDialog.qml:8
#: /hdd/projects/rockwork/rockwork/qml/dialogs/ConfirmDialog.qml:8
#: /hdd/projects/rockwork/rockwork/qml/dialogs/HealthSettingsDialog.qml:95
#: /hdd/projects/rockwork/rockwork/qml/dialogs/PromptDialog.qml:14
#: /hdd/projects/rockwork/rockwork/qml/dialogs/WeatherSettingsDialog.qml:57
msgid "OK"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/BeforeUnloadDialog.qml:5
msgid "App Settings Confirm Navigation"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/BeforeUnloadDialog.qml:8
msgid "Leave"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/BeforeUnloadDialog.qml:13
msgid "Stay"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/ConfirmDialog.qml:5
msgid "App Settings Confirmation"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/ConfirmDialog.qml:13
#: /hdd/projects/rockwork/rockwork/qml/dialogs/HealthSettingsDialog.qml:110
#: /hdd/projects/rockwork/rockwork/qml/dialogs/PromptDialog.qml:20
#: /hdd/projects/rockwork/rockwork/qml/dialogs/SendLogsDialog.qml:55
#: /hdd/projects/rockwork/rockwork/qml/dialogs/SendTextSettingsDialog.qml:33
#: /hdd/projects/rockwork/rockwork/qml/dialogs/WeatherSettingsDialog.qml:65
#: /hdd/projects/rockwork/rockwork/qml/pages/ScreenshotsPage.qml:100
msgid "Cancel"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/HealthSettingsDialog.qml:9
msgid "Health settings"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/HealthSettingsDialog.qml:17
msgid "Health app enabled"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/HealthSettingsDialog.qml:28
msgid "Female"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/HealthSettingsDialog.qml:28
msgid "Male"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/HealthSettingsDialog.qml:34
msgid "Age"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/HealthSettingsDialog.qml:47
msgid "Height (cm)"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/HealthSettingsDialog.qml:60
msgid "Weight"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/HealthSettingsDialog.qml:73
msgid "I want to be more active"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/HealthSettingsDialog.qml:84
msgid "I want to sleep more"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/PromptDialog.qml:5
msgid "App Settings Prompt"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/SendLogsDialog.qml:8
#: /hdd/projects/rockwork/rockwork/qml/pages/InfoPage.qml:46
msgid "Report problem"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/SendLogsDialog.qml:18
msgid "Preparing logs package..."
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/SendLogsDialog.qml:29
msgid "pebble.log"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/SendLogsDialog.qml:36
msgid "Send rockworkd.log"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/SendLogsDialog.qml:41
msgid "rockworkd.log"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/SendLogsDialog.qml:46
msgid "Send watch logs"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/SendTextSettingsDialog.qml:13
msgid "Contacts"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/SendTextSettingsDialog.qml:23
msgid "Messages"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/WeatherSettingsDialog.qml:30
msgid "Units"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/WeatherSettingsDialog.qml:31
msgid "Celsius"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/WeatherSettingsDialog.qml:32
msgid "Fahrenheit"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/WeatherSettingsDialog.qml:39
msgid "Locations"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/dialogs/WeatherSettingsDialog.qml:49
msgid "Search"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/graphs/DailySleepGraph.qml:113
msgid "Oops! No sleep data."
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/graphs/DailyStepsGraph.qml:112
msgid "TODAY'S STEPS"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/graphs/DailyStepsGraph.qml:114
msgid "YESTERDAY'S STEPS"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/graphs/DailyStepsGraph.qml:116
msgid "STEPS"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/graphs/DailyStepsGraph.qml:119
msgid "TYPICAL"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/graphs/MonthlyStepsGraph.qml:125
#: /hdd/projects/rockwork/rockwork/qml/graphs/WeeklyStepsGraph.qml:95
msgid "AVERAGE STEPS"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/graphs/MonthlyStepsGraph.qml:125
#: /hdd/projects/rockwork/rockwork/qml/graphs/WeeklyStepsGraph.qml:95
#, qt-format
msgid "TYPICAL %1"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/graphs/WeeklySleepGraph.qml:119
msgid "AVERAGE SLEEP"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/graphs/WeeklySleepGraph.qml:119
#, qt-format
msgid "TYPICAL %1H %2M"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppSettingsPage.qml:16
msgid "App Settings"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppSettingsPage.qml:139
msgid "Cut"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppSettingsPage.qml:151
msgid "Copy"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppSettingsPage.qml:163
msgid "Paste"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppSettingsPage.qml:176
#: /hdd/projects/rockwork/rockwork/qml/pages/DeveloperToolsPage.qml:38
#: /hdd/projects/rockwork/rockwork/qml/pages/InstalledAppsPage.qml:323
msgid "Close"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppStoreDetailsPage.qml:9
msgid "App details"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppStoreDetailsPage.qml:57
msgid "Install"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppStoreDetailsPage.qml:57
msgid "Installing..."
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppStoreDetailsPage.qml:57
msgid "Installed"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppStoreDetailsPage.qml:241
msgid "Description"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppStoreDetailsPage.qml:264
msgid "Developer"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppStorePage.qml:25
msgid "Add new watchapp"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppStorePage.qml:25
msgid "Add new watchface"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppStorePage.qml:134
msgid "See all"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppUpgradePage.qml:24
#: /hdd/projects/rockwork/rockwork/qml/pages/AppUpgradePage.qml:25
msgid "Upgrading"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppUpgradePage.qml:77
#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:223
msgid "Upgrading..."
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppUpgradePage.qml:77
msgid "Needs Companion"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppUpgradePage.qml:93
msgid "Compatibility"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/AppUpgradePage.qml:175
msgid "Change Log"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/DeveloperConnectionPage.qml:11
msgid "Developer Connection"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/DeveloperConnectionPage.qml:30
msgid "Enable developer connection"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/DeveloperConnectionPage.qml:40
msgid "Developer connection port:"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/DeveloperConnectionPage.qml:53
msgid "Developer connection server status: "
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/DeveloperConnectionPage.qml:63
msgid "Status:"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/DeveloperConnectionPage.qml:68
msgid "Running..."
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/DeveloperConnectionPage.qml:68
msgid "Stopped"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/DeveloperConnectionPage.qml:74
msgid "Port:"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/DeveloperToolsPage.qml:12
msgid "Developer Tools"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/DeveloperToolsPage.qml:33
#: /hdd/projects/rockwork/rockwork/qml/pages/DeveloperToolsPage.qml:71
msgid "Ping watch"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/DeveloperToolsPage.qml:50
msgid "Install app or watchface from file"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/DeveloperToolsPage.qml:57
msgid "Install firmware from file"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/DeveloperToolsPage.qml:64
msgid "Developer Connection settings"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/FirmwareUpgradePage.qml:6
msgid "Firmware upgrade"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/FirmwareUpgradePage.qml:16
msgid "A new firmware upgrade is available for your Pebble smartwatch."
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/FirmwareUpgradePage.qml:23
#, qt-format
msgid "Currently installed firmware: %1"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/FirmwareUpgradePage.qml:29
#, qt-format
msgid "Candidate firmware version: %1"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/FirmwareUpgradePage.qml:35
#, qt-format
msgid "Release Notes: %1"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/FirmwareUpgradePage.qml:41
msgid "Important:"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/FirmwareUpgradePage.qml:41
msgid ""
"This update will also upgrade recovery data. Make sure your Pebble "
"smartwarch is connected to a power adapter."
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/HealthPage.qml:12
msgid "Health info"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/HealthPage.qml:157
msgid "Enable health data to see health statistics."
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/ImportFirmware.qml:7
msgid "Install firmware"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/ImportPackagePage.qml:7
msgid "Import watchapp or watchface"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/InfoPage.qml:35
#, qt-format
msgid "Version %1"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/InfoPage.qml:57
msgid "Legal"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/InfoPage.qml:79
msgid ""
"This application is neither affiliated with nor endorsed by Pebble "
"Technology Corp."
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/InfoPage.qml:84
msgid "Pebble is a trademark of Pebble Technology Corp."
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/InstalledAppsPage.qml:19
msgid "Apps & Watchfaces"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/InstalledAppsPage.qml:19
msgid "Apps"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/InstalledAppsPage.qml:19
msgid "Watchfaces"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/InstalledAppsPage.qml:293
msgid "Set as watchface"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/InstalledAppsPage.qml:293
msgid "Launch"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/InstalledAppsPage.qml:303
msgid "Configure"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/InstalledAppsPage.qml:313
#: /hdd/projects/rockwork/rockwork/qml/pages/ScreenshotsPage.qml:92
msgid "Delete"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/LanguagePage.qml:20
#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:66
msgid "Language Settings"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/LanguagePage.qml:45
msgid "Select Language:"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/NotificationsPage.qml:9
msgid "Notifications"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/NotificationsPage.qml:37
msgid ""
"Entries here will be added as notifications appear on the phone. Selected "
"notifications will be shown on your Pebble smartwatch."
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/NotificationsPage.qml:94
msgid "Forget"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:17
msgid "About"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:24
#: /hdd/projects/rockwork/rockwork/qml/pages/ScreenshotsPage.qml:13
msgid "Screenshots"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:31
msgid "Developer tools"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:69
#: /hdd/projects/rockwork/rockwork/qml/pages/PebblesPage.qml:38
msgid "Connected"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:69
#: /hdd/projects/rockwork/rockwork/qml/pages/PebblesPage.qml:38
msgid "Disconnected"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:180
msgid ""
"Your Pebble smartwatch is disconnected. Please make sure it is powered on, "
"within range and it is paired properly in the Bluetooth System Settings."
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:188
#: /hdd/projects/rockwork/rockwork/qml/pages/PebblesPage.qml:66
msgid "Open System Settings"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:207
msgid "Your Pebble smartwatch is in factory mode and needs to be initialized."
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:215
msgid "Initialize Pebble"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:265
msgid "No steps today"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:267
#, qt-format
msgid "%1 steps today"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:293
#, qt-format
msgid "Slept %1 hour"
msgid_plural "Slept %1 hours"
msgstr[0] ""
msgstr[1] ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:294
#, qt-format
msgid ", %1 minute"
msgid_plural ", %1 minutes"
msgstr[0] ""
msgstr[1] ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:300
msgid " today"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:303
#, qt-format
msgid "Slept %1 minute today"
msgid_plural "Slept %1 minutes today"
msgstr[0] ""
msgstr[1] ""

#: /hdd/projects/rockwork/rockwork/qml/pages/OverviewPage.qml:306
msgid "No sleep today"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/PebblesPage.qml:9
msgid "Manage Pebble Watches"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/PebblesPage.qml:59
msgid ""
"No Pebble smartwatches configured yet. Please connect your Pebble smartwatch "
"using System Settings."
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/ScreenshotsPage.qml:70
msgid "Screenshot options"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/ScreenshotsPage.qml:75
msgid "Share"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/ScreenshotsPage.qml:78
#: /hdd/projects/rockwork/rockwork/qml/pages/ScreenshotsPage.qml:86
msgid "Pebble screenshot"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/ScreenshotsPage.qml:83
msgid "Save"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:12
msgid "Settings"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:27
msgid "Distance Units"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:43
msgid "Metric"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:56
msgid "Imperial"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:72
msgid "Change Language"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:83
#: /hdd/projects/rockwork/rockwork/qml/pages/VoiceSettingsPage.qml:12
msgid "Voice Settings"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:89
msgid "Change Voice Settings"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:99
msgid "Timeline"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:106
msgid "Sync calendar to timeline"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:120
msgid "Sync Apps from Cloud"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:131
msgid "Reset Timeline"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:137
msgid "Timeline Window Start (days ago)"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:151
msgid "Timeline Window End (days ahead)"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:165
#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:170
msgid "Notification re-delivery expiration (seconds)"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:181
msgid "Set Timeline Window"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:191
msgid "Active Timeline WebSync account"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:212
msgid "Logout"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/SettingsPage.qml:212
msgid "Login"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/VoiceSettingsPage.qml:31
msgid "Enable voice transcription"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/VoiceSettingsPage.qml:44
msgid "Languages"
msgstr ""

#: /hdd/projects/rockwork/rockwork/qml/pages/VoiceSettingsPage.qml:59
msgid "Languages unavailable. Are you signed in?"
msgstr ""
