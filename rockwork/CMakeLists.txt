set(PROJECT_NAME "rockwork")

find_package(Qt5 COMPONENTS Quick Qml DBus Gui REQUIRED)

set (DESKTOP_FILE_NAME "rockwork.desktop")

set (SOURCES 
    "main.cpp"
    "applicationsfiltermodel.cpp"
    "applicationsmodel.cpp"
    "appstoreclient.cpp"
    "notificationsourcemodel.cpp"
    "pebble.cpp"
    "pebbles.cpp"
    "screenshotmodel.cpp"
    "servicecontrol.cpp"

    "qml/qml.qrc"
    "rockwork.qrc"
)

add_custom_target(${DESKTOP_FILE_NAME} ALL
    COMMENT "Merging translations into ${DESKTOP_FILE_NAME}..."
    COMMAND LC_ALL=C ${INTLTOOL_MERGE} -d -u ${CMAKE_SOURCE_DIR}/po ${CMAKE_SOURCE_DIR}/rockwork/data/${DESKTOP_FILE_NAME} ${DESKTOP_FILE_NAME}
    COMMAND sed -i 's/${PROJECT_NAME}-//g' ${CMAKE_CURRENT_BINARY_DIR}/${DESKTOP_FILE_NAME}
)

add_definitions(-DVERSION=\"${VERSION}\")
add_executable(${PROJECT_NAME} ${SOURCES})
target_link_libraries(${PROJECT_NAME} Qt5::Qml Qt5::DBus Qt5::Gui Qt5::Quick)

install(FILES data/${PROJECT_NAME}.apparmor DESTINATION ${DATA_DIR})
install(FILES data/${PROJECT_NAME}.svg DESTINATION ${DATA_DIR})
install(FILES data/${PROJECT_NAME}.url-dispatcher DESTINATION ${DATA_DIR})
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${DESKTOP_FILE_NAME} DESTINATION ${DATA_DIR})

install(TARGETS ${PROJECT_NAME} RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})