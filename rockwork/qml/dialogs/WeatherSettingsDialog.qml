import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

// Location
import QtLocation 5.12
import QtPositioning 5.12

Dialog {
    id: weatherSettingsDialog
    title: "Weather Settings"

    property var params: null
    property var locations: []

    PositionSource {
        id: currentPosition
        updateInterval: 1000
        active: true

        onPositionChanged: {
            var coord = currentPosition.position.coordinate
            if (coord.isValid) {
                print("Got response", coord)
            }
        }
    }

    OptionSelector {
        text: i18n.tr("Units")
        model: [i18n.tr("Celsius"),
                i18n.tr("Fahrenheit")]
        onDelegateClicked: {
            weatherUnit = selectedIndex
        }
    }

    Label {
        text: i18n.tr("Locations")
        font.bold: true
    }

    Label {
        width: parent.width
        text: "Not implemented yet"
    }

    Button {
        text: i18n.tr("Search")
        color: LomiriColors.blue
        onClicked: {
            print("hewo!!")
        }
    }

    Button {
        text: i18n.tr("OK")
        color: LomiriColors.green
        onClicked: {
            PopupUtils.close(weatherSettingsDialog);
        }
    }

    Button {
        text: i18n.tr("Cancel")
        color: LomiriColors.red
        onClicked: PopupUtils.close(weatherSettingsDialog)
    }
}
