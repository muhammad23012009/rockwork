import QtQuick 2.4
import Lomiri.Components 1.3
import RockWork 1.0

HealthGraph {
    id: root

    onDateChanged: {
        print("should update steps graph", date)
        if (date === null) {
            return;
        }

        var startDate = new Date(date);
        startDate.setDate(startDate.getDate() - startDate.getDay() + 1);
        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);

        d.maxSteps = 0
        d.averageSteps = 0;
        let entries = root.pebble.stepsDataForWeek(startDate);

        d.stepsList = entries.steps;
        d.averageSteps = entries.averageSteps;
        d.maxSteps = Math.max(d.maxSteps, entries.maxSteps);

        d.averageDistance = Math.round(entries.averageDistance / 10000) / 10;
        d.averageCalories = entries.averageCalories / 1000
        d.averageActiveTime = entries.averageActiveTime;

        root.infoRowModel.clear();
        root.infoRowModel.append({type: "distance", title: "Avg Distance", distance: d.averageDistance})
        root.infoRowModel.append({type: "calories", title: "Avg Calories", calories: d.averageCalories})
        root.infoRowModel.append({type: "activetime", title: "Avg Active", activeTime: d.averageActiveTime})

        requestPaint();
    }

    QtObject {
        id: d
        property var stepsList: []
        property int typicalSteps: 0
        property int averageSteps: 0
        property int maxSteps: 0

        property double averageDistance: 0
        property int averageCalories: 0
        property int averageActiveTime: 0
    }

    onPaint: {
        var ctx = getContext('2d');
        ctx.save();
        ctx.reset()

        var slotsWidth = root.width / 7;
        var dayNames = ["M", "T", "W", "T", "F", "S", "S"]

        ctx.beginPath();
        ctx.fillStyle = "yellow"
        // avg : ms = y : r.h
        var avgY = root.height - d.typicalSteps * (root.height / 2) / d.maxSteps + units.gu(.25)
        ctx.rect(0, avgY, root.width, units.gu(.5))
        ctx.fill();
        ctx.closePath();

        ctx.font = "bold " + units.gu(1.5) + "px Ubuntu";

        for (var i = 0; i < 7; i++) {
            ctx.beginPath();
            ctx.fillStyle = Qt.darker(root.baseColor, 1.5)
            var height = slotsWidth - units.gu(2)
            roundRect(ctx, slotsWidth * i + units.gu(1), root.height - height, slotsWidth - units.gu(2), height, units.dp(3), true, false)
            ctx.fill();
            ctx.closePath();

            if (d.stepsList[i].steps >= 0) {
                ctx.beginPath();
                ctx.fillStyle = Qt.lighter(root.baseColor, 1.2)
                // s : ms = h : r.h
                height = d.stepsList[i].steps * root.height / 2 / d.maxSteps
                roundRect(ctx, slotsWidth * i + units.gu(1), root.height - height, slotsWidth - units.gu(2), height, units.dp(3), true, false)
                ctx.fill();
                ctx.closePath();
            }
            ctx.beginPath();
            ctx.fillStyle = "white"
            var textWidth = ctx.measureText(dayNames[i]).width
            ctx.text(dayNames[i], slotsWidth * i + (slotsWidth - textWidth) / 2, root.height - units.gu(1))
            ctx.fill();
            ctx.closePath();
        }

        paintCounterSummary(ctx, root.width / 2, units.gu(2), d.averageSteps, i18n.tr("AVERAGE STEPS"), i18n.tr("TYPICAL %1").arg(d.typicalSteps), root.baseColor)
        ctx.restore();
    }
}

