import QtQuick 2.4
import Lomiri.Components 1.3

HealthGraph {
    id: root

    onDateChanged: {
        print("should update steps graph", date)

        var startTime = date;
        startTime.setHours(0);
        startTime.setMinutes(0);
        startTime.setSeconds(0);

        var endTime = new Date(startTime);
        endTime.setDate(endTime.getDate() + 1);

        d.stepsList = {};
        d.maxSteps = 0;
        d.stepsCalories = 0;
        d.stepsDistance = 0;
        d.activeStepsTime = 0;
        var stepsDistance = 0;

        var entryList = root.pebble.stepsDataForDay(startTime, endTime);
        d.stepsList = entryList.steps;

        d.steps = entryList.totalSteps;
        d.maxSteps = Math.max(d.maxSteps, entryList.totalSteps);

        d.stepsCalories = (entryList.active_calories + entryList.resting_calories) / 1000.0
        d.activeStepsTime = entryList.active_minutes;
        d.stepsDistance = Math.round(entryList.distance_cm / 10000) / 10;

        d.stepsAverageList = {};

        var previousStartTime = new Date(startTime);
        previousStartTime.setDate(previousStartTime.getDate() - 1);
        var previousEndTime = new Date(previousStartTime);
        previousEndTime.setDate(previousEndTime.getDate() + 1);

        entryList = root.pebble.stepsDataForDay(previousStartTime, previousEndTime);
        d.stepsAverageList = entryList.steps;

        d.maxSteps = Math.max(d.maxSteps, entryList.totalSteps);

        root.infoRowModel.clear();
        root.infoRowModel.append({type: "distance", title: "Distance", distance: d.stepsDistance})
        root.infoRowModel.append({type: "calories", title: "Calories", calories: d.stepsCalories})
        root.infoRowModel.append({type: "activetime", title: "Active Time", activeTime: d.activeStepsTime})

        requestPaint();
    }

    QtObject {
        id: d
        property int steps: 0
        property var stepsList: {}
        property var stepsAverageList: {}
        property int maxSteps: 0
        property int stepsCalories: 0
        property double stepsDistance: 0
        property int activeStepsTime: 0

        property int startSlot: 8
        property real slotWidth: root.width / (24 - startSlot);
    }

    onPaint: {
        var ctx = getContext('2d');
        ctx.save();
        ctx.reset();

        ctx.font = "" + units.gu(1.5) + "px Ubuntu";
        ctx.lineWidth = 0

        var points = [];

        // Average steps
        ctx.beginPath();
        ctx.fillStyle = Qt.darker("yellow", 1.3)
        paintDailyStepsGraph(ctx, d.stepsAverageList, d.slotWidth, d.maxSteps, true)
        ctx.closePath();


        // Steps
        ctx.beginPath();
        ctx.fillStyle = Qt.lighter(root.baseColor, 1.2)
        paintDailyStepsGraph(ctx, d.stepsList, d.slotWidth, d.maxSteps, true)
        ctx.closePath();

        ctx.beginPath();
        ctx.strokeStyle = Qt.darker(root.baseColor, 1.2)
        ctx.lineWidth = units.gu(.3)
        paintDailyStepsGraph(ctx, d.stepsAverageList, d.slotWidth, d.maxSteps, false)
        ctx.closePath();

        ctx.beginPath();
        ctx.font = "bold " + units.gu(1) + "px Ubuntu";
        ctx.fillStyle = "white";
        for (var i = 12; i < 24; i+=4) {
            ctx.text(i + ":00", (i - d.startSlot) * d.slotWidth, root.height - units.gu(.5));
            ctx.fill()
        }
        ctx.closePath();

        var today = new Date();
        var yesterday = new Date()
        yesterday.setDate(today.getDate() - 1)
        var text;
        if (today.getDate() == date.getDate() && today.getDay() == date.getDay()) {
            text = i18n.tr("TODAY'S STEPS");
        } else if (yesterday.getDate() == date.getDate() && yesterday.getDay() == date.getDay()) {
            text = i18n.tr("YESTERDAY'S STEPS")
        } else {
            text = i18n.tr("STEPS")
        }

        paintCounterSummary(ctx, root.width / 2, units.gu(2), d.steps, text, i18n.tr("TYPICAL"), root.baseColor)

        ctx.restore();
    }
}

