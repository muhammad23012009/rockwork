import QtQuick 2.4
import Lomiri.Components 1.3

HealthGraph {
    id: root

    onDateChanged: {
        print("should update sleep graph", date)
        if (date === null) {
            return;
        }

        var startDate = new Date(date);
        startDate.setDate(startDate.getDate() - startDate.getDay() + 1);
        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);

        var entryList = root.pebble.sleepDataForWeek(startDate);

        d.sleepList = entryList.sleep;
        d.maxSleep = entryList.maxDuration;
        d.averageSleep = entryList.averageDuration;

        for (let i = 0; i < d.sleepList.length; i++) {
            print("meow testing weekly sleep", d.sleepList[i].sleepDuration, d.sleepList.length, d.sleepList[i].deepSleepDuration);
        }

        // fetch typical sleep as weekday average
        var averageStartDate = new Date(startDate)
        averageStartDate.setDate(averageStartDate.getDate() - 30);
        var averageEndDate = new Date(startDate)
        averageEndDate.setDate(averageEndDate.getDate() + 6)
        var lastWeekDay = new Date(startDate);
        lastWeekDay.setDate(lastWeekDay.getDate() + 4)
        var averageSleepTimes = root.pebble.averageSleepTimes(lastWeekDay);
        d.averageSleepTime = averageSleepTimes.fallasleep || -1;
        d.averageWakeupTime = averageSleepTimes.wakeup || -1;

        // the "1" is normal sleep, "2" is deep sleep
        d.typicalSleep = root.pebble.sleepAverage(averageStartDate, averageEndDate, 1)
        var averageDeepSleepDuration = root.pebble.sleepAverage(averageStartDate, averageEndDate, 2)

        var avgFallAsleep = new Date(d.averageSleepTime * 1000);
        var avgWakeup = new Date(d.averageWakeupTime * 1000);

        root.infoRowModel.clear();
        root.infoRowModel.append({type: "duration", title: "Avg Deep Sleep", duration: averageDeepSleepDuration})
        root.infoRowModel.append({type: "time", title: "Avg Fall Asleep", time: pad(avgFallAsleep.getHours(), 2) + ":" + pad(avgFallAsleep.getMinutes(), 2)})
        root.infoRowModel.append({type: "time", title: "Avg Wakeup", time: pad(avgWakeup.getHours(), 2) + ":" + pad(avgWakeup.getMinutes(), 2)})

        requestPaint();
    }

    QtObject {
        id: d
        property var sleepList: {}
        property int typicalSleep: 0
        property int averageSleep: 0
        property int maxSleep: 0
        property int averageSleepTime : 0
        property int averageWakeupTime: 0
    }

    onPaint: {
        var ctx = getContext('2d');
        ctx.save();
        ctx.reset()

        var slotsWidth = root.width / 7;
        var dayNames = ["M", "T", "W", "T", "F", "S", "S"]

        ctx.beginPath();
        ctx.fillStyle = "yellow"
        // avg : ms = y : r.h
        var avgY = root.height - d.typicalSleep * root.height / 2 / d.maxSleep + units.gu(.25)
        ctx.rect(0, avgY, root.width, units.gu(.5))
        ctx.fill();
        ctx.closePath();

        ctx.font = "bold " + units.gu(1.5) + "px Ubuntu";

        for (var i = 0; i < 7; i++) {
            ctx.beginPath();
            ctx.fillStyle = Qt.darker(root.baseColor, 1.5)
            var height = slotsWidth - units.gu(2)
            roundRect(ctx, slotsWidth * i + units.gu(1), root.height - height, slotsWidth - units.gu(2), height, units.dp(3), true, false)
            ctx.fill();
            ctx.closePath();

            if (d.sleepList[i].sleepDuration >= 0) {
                ctx.beginPath();
                ctx.fillStyle = Qt.lighter(root.baseColor, 1.4)
                // s : ms = h : r.h
                height = d.sleepList[i].sleepDuration * root.height / 2 / d.maxSleep
                roundRect(ctx, slotsWidth * i + units.gu(1), root.height - height, slotsWidth - units.gu(2), height, units.dp(3), true, false)
                ctx.fill();
                ctx.closePath();
            }

            if (d.sleepList[i].deepSleepDuration >= 0) {
                ctx.beginPath();
                ctx.fillStyle = Qt.lighter(root.baseColor, 1.2)
                // s : ms = h : r.h
                height = d.sleepList[i].deepSleepDuration * root.height / 2 / d.maxSleep
                roundRect(ctx, slotsWidth * i + units.gu(1), root.height - height, slotsWidth - units.gu(2), height, units.dp(3), true, false)
                ctx.fill();
                ctx.closePath();
            }

            ctx.beginPath();
            ctx.fillStyle = "white"
            var textWidth = ctx.measureText(dayNames[i]).width
            ctx.text(dayNames[i], slotsWidth * i + (slotsWidth - textWidth) / 2, root.height - units.gu(1))
            ctx.fill();
            ctx.closePath();
        }

        paintDurationSummary(ctx, root.width / 2, units.gu(2), d.averageSleep, i18n.tr("AVERAGE SLEEP"), i18n.tr("TYPICAL %1H %2M").arg(Math.floor(d.typicalSleep / 60 / 60)).arg(Math.round(d.typicalSleep / 60 % 60)), root.baseColor)
        ctx.restore();
    }

}

