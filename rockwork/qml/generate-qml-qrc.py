import glob

header = """
<RCC>
    <qresource prefix="/">
"""

trailer = """
    </qresource>
</RCC>
"""

def get_qml_pages():
    return glob.glob("**/*.qml", recursive=True)

def generate_qrc(files):
    with open("qml.qrc", "w") as output:
        output.write(header)

        for file in files:
            alias = file.split("/")
            if len(alias) < 2:
                alias = alias[0]
            else:
                alias = alias[1]

            output.write(f"        <file alias=\"{alias}\">{file}</file>\n")
        
        output.write(trailer)

if __name__ == "__main__":
    generate_qrc(get_qml_pages())