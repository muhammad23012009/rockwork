import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Page {
    id: root
    anchors.fill: parent

    header: PageHeader {
        id: header
        title: i18n.tr("Developer Tools")
        flickable: pageFlickable
    }

    property var pebble: null

    //Creating the menu list this way to allow the text field to be translatable (http://askubuntu.com/a/476331)
    ListModel {
        id: devMenuModel
        dynamicRoles: true
    }

    Component.onCompleted: {
        populateDevMenu();
    }

    Component {
        id: pingDialog
        Dialog {
            id: dialog
            Button {
                text: i18n.tr("Ping watch")
                color: LomiriColors.blue
                onClicked: root.pebble.ping()
            }
            Button {
                text: i18n.tr("Close")
                color: LomiriColors.red
                onClicked: PopupUtils.close(dialog)
            }
        }
    }

    function populateDevMenu() {
        devMenuModel.clear();

        devMenuModel.append({
            icon: "stock_application",
            text: i18n.tr("Install app or watchface from file"),
            page: "ImportPackagePage.qml",
            dialog: null,
            color: LomiriColors.blue
        });
        devMenuModel.append({
            icon: "stock_application",
            text: i18n.tr("Install firmware from file"),
            page: "ImportFirmware.qml",
            dialog: null,
            color: LomiriColors.green
        });
        devMenuModel.append({
            icon: "ubuntu-sdk-symbolic",
            text: i18n.tr("Developer Connection settings"),
            page: "DeveloperConnectionPage.qml",
            dialog: null,
            color: LomiriColors.ash
        });
        devMenuModel.append({
            icon: "toolkit_arrow-right",
            text: i18n.tr("Ping watch"),
            dialog: pingDialog,
            color: LomiriColors.slate
        });
    }

    Flickable {
        id: pageFlickable
        anchors.fill: parent

        ColumnLayout {
            anchors.fill: parent

            Repeater {
                id: menuRepeater
                model: devMenuModel
                delegate: ListItem {

                    RowLayout {
                        anchors.fill: parent
                        anchors.margins: units.gu(1)

                        LomiriShape {
                            Layout.fillHeight: true
                            Layout.preferredWidth: height
                            backgroundColor: model.color
                                Icon {
                                    anchors.fill: parent
                                    anchors.margins: units.gu(.5)
                                    name: model.icon
                                    color: "white"
                                }
                        }


                        Label {
                            text: model.text
                            Layout.fillWidth: true
                        }
                    }

                    onClicked: {
                        if (model.page) {
                            pageStack.push(Qt.resolvedUrl(model.page), {pebble: root.pebble})
                        }
                        if (model.dialog) {
                            PopupUtils.open(model.dialog)
                        }
                    }
                }
            }

            Item {
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
        }
    }
}
