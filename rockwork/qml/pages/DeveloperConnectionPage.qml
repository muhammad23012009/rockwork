import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3

Page {
    id: root
    property var pebble: null

    header: PageHeader {
        title: i18n.tr("Developer Connection")
        flickable: myFlickable
    }

    Flickable {
        id: myFlickable
        topMargin: root.header.flickable ? 0 : root.header.height
        contentHeight: contentLayout.height
        anchors.fill: parent

        ColumnLayout {
            id: contentLayout
            anchors.fill: parent
            anchors.margins: units.gu(1)
            spacing: units.gu(2)

            RowLayout {
                Layout.fillWidth: true
                Label {
                    text: i18n.tr("Enable developer connection")
                    Layout.fillWidth: true
                }
                Switch {
                    checked: root.pebble.devConnEnabled
                    onClicked: root.pebble.devConnEnabled = checked
                }
            }

            Label {
                text: i18n.tr("Developer connection port:")
                font.bold: true
                Layout.fillWidth: true
            }
            TextField {
                Layout.fillWidth: true
                text: root.pebble.devConListenPort
                onAccepted: {
                    root.pebble.devConListenPort = parseInt(text);
                }
            }

            Label {
                text: i18n.tr("Developer connection server status: ")
                Layout.fillWidth: true
                font.bold: true
            }

            ColumnLayout {
                Layout.fillWidth: true
                RowLayout {
                    Layout.fillWidth: true
                    Label {
                        text: i18n.tr("Status:")
                        Layout.fillWidth: true
                        font.bold: true
                    }
                    Label {
                        text: root.pebble.devConnServerRunning ? i18n.tr("Running...") : i18n.tr("Stopped")
                    }
                }
                RowLayout {
                    Layout.fillWidth: true
                    Label {
                        text: i18n.tr("Port:")
                        Layout.fillWidth: true
                        font.bold: true
                    }
                    Label {
                        text: root.pebble.devConListenPort
                    }
                }
            }

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
        }
    }
}