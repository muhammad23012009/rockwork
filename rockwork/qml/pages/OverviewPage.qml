import QtQuick 2.4
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.1
import RockWork 1.0
import Lomiri.Components 1.3
import UserMetrics 0.1

Item {
    id: root
    property var pebble: null
    property bool focused: false

    property string title: pebble.name
    property list<Action> actions: [
        Action {
            iconName: "info"
            text: i18n.tr("About")
            onTriggered: {
                pageStack.push(Qt.resolvedUrl("InfoPage.qml"), {pebble: root.pebble})
            }
        },
        Action {
            iconName: "camera-app-symbolic"
            text: i18n.tr("Screenshots")
            onTriggered: {
                pageStack.push(Qt.resolvedUrl("ScreenshotsPage.qml"), {pebble: root.pebble})
            }
        },
        Action {
            iconName: "ubuntu-sdk-symbolic"
            text: i18n.tr("Developer tools")
            visible: root.pebble.connected
            onTriggered: {
                pageStack.push(Qt.resolvedUrl("DeveloperToolsPage.qml"), {pebble: root.pebble})
            }
        }
    ]


    PebbleModels {
        id: modelModel
    }

    GridLayout {
        id: grid
        anchors.fill: parent
        flow: GridLayout.TopToBottom

        RowLayout {
            Layout.alignment: Qt.AlignTop
            Layout.fillWidth: true
            Layout.margins: units.gu(2.5)
            spacing: units.gu(1)

            Rectangle {
                height: units.gu(5)
                width: height
                radius: height / 2
                color: root.pebble.connected ? LomiriColors.green : LomiriColors.red

                Icon {
                    anchors.fill: parent
                    anchors.margins: units.gu(1)
                    color: "white"
                    name: root.pebble.connected ? "tick" : "dialog-error-symbolic"
                }
            }
            Label {
                text: root.pebble.connected ? i18n.tr("Connected") : i18n.tr("Disconnected")
            }
            
            Item { Layout.fillWidth: true } // spacer

            Icon {
                width: units.gu(5)
                height: width
                name: "battery-" + (root.pebble.batteryPercentage < 5 ? "000" : (root.pebble.batteryPercentage < 95 ? "0" + (Math.round(root.pebble.batteryPercentage/10) * 10) : (Math.round(root.pebble.batteryPercentage/10) * 10)))
                color: theme.palette.normal.foregroundText
                visible: root.pebble.batteryPercentage != 0
            }
            Label {
                text: root.pebble.batteryPercentage + "%"
                visible: root.pebble.batteryPercentage != 0
            }
        }

        Item {
            Layout.alignment: Qt.AlignCenter
            Layout.minimumHeight: watchImage.height
            Layout.minimumWidth: watchImage.width

            Image {
                id: watchImage
                width: units.gu(35)
                height: width // Is 35 grid units fine?
                anchors.horizontalCenter: parent.horizontalCenter

                source: modelModel.get(root.pebble.model).image
                fillMode: Image.PreserveAspectFit

                Item {
                    id: watchFace
                    height: parent.height * (modelModel.get(root.pebble.model).shape === "rectangle" ? .5 : .515)
                    width: height * (modelModel.get(root.pebble.model).shape === "rectangle" ? .85 : 1)
                    anchors.centerIn: parent
                    anchors.horizontalCenterOffset: units.dp(1)
                    anchors.verticalCenterOffset: units.dp(modelModel.get(root.pebble.model).shape === "rectangle" ? 0 : 1)

                    Image {
                        id: image
                        anchors.fill: parent
                        source: "file://" + root.pebble.screenshots.latestScreenshot
                        visible: false
                    }

                    Component.onCompleted: {
                        if (!root.pebble.screenshots.latestScreenshot) {
                            root.pebble.requestScreenshot();
                        }
                    }

                    Rectangle {
                        id: textItem
                        anchors.fill: parent
                        layer.enabled: true
                        radius: modelModel.get(root.pebble.model).shape === "rectangle" ? units.gu(.5) : height / 2
                        // This item should be used as the 'mask'
                        layer.samplerName: "maskSource"
                        layer.effect: ShaderEffect {
                            property var colorSource: image;
                            fragmentShader: "
                                uniform lowp sampler2D colorSource;
                                uniform lowp sampler2D maskSource;
                                uniform lowp float qt_Opacity;
                                varying highp vec2 qt_TexCoord0;
                                void main() {
                                    gl_FragColor =
                                        texture2D(colorSource, qt_TexCoord0)
                                        * texture2D(maskSource, qt_TexCoord0).a
                                        * qt_Opacity;
                                }
                            "
                        }
                    }
                }
            }
        }

        RowLayout {
            id: healthOverview
            visible: root.pebble.connected && root.pebble.healthSupported && root.pebble.healthEnabled && !root.pebble.recovery
            Layout.alignment: Qt.AlignBottom
            Layout.fillWidth: visible
            Layout.margins: units.gu(2.5)
            spacing: units.gu(1)

            Label {
                id: steps
                text: calculateOverviewSteps()
            }

            Item { Layout.fillWidth: true } // spacer

            Label {
                id: sleep
                text: calculateOverviewSleep()
            }
        }

        ColumnLayout {
            visible: !root.pebble.connected
            Layout.fillWidth: true
            Layout.minimumHeight: childrenRect.height
            Layout.alignment: Qt.AlignBottom | Qt.AlignCenter
            Layout.margins: units.gu(2)
            spacing: units.gu(1)

            Label {
                Layout.fillWidth: true
                text: i18n.tr("Your Pebble smartwatch is disconnected. Please make sure it is powered on, within range and it is paired properly in the Bluetooth System Settings.")
                wrapMode: Text.WordWrap
                visible: !root.pebble.connected
                fontSize: "large"
                horizontalAlignment: Text.AlignHCenter
                Layout.alignment: Qt.AlignCenter
            }
            Button {
                text: i18n.tr("Open System Settings")
                visible: !root.pebble.connected
                onClicked: Qt.openUrlExternally("settings://system/bluetooth")
                color: LomiriColors.orange
                Layout.alignment: Qt.AlignCenter
            }
        }

        // Upgrading info
        ColumnLayout {
            visible: root.pebble.connected && root.pebble.recovery
            Layout.fillWidth: true
            Layout.minimumHeight: childrenRect.height
            Layout.alignment: Qt.AlignBottom | Qt.AlignCenter
            Layout.margins: units.gu(2)
            spacing: units.gu(1)

            Label {
                Layout.fillWidth: true
                text: i18n.tr("Your Pebble smartwatch is in factory mode and needs to be initialized.")
                wrapMode: Text.WordWrap
                visible: root.pebble.connected && root.pebble.recovery && !root.pebble.upgradingFirmware
                fontSize: "large"
                Layout.alignment: Qt.AlignCenter
                horizontalAlignment: Text.AlignHCenter
            }
            Button {
                text: i18n.tr("Initialize Pebble")
                onClicked: root.pebble.performFirmwareUpgrade();
                visible: root.pebble.connected && root.pebble.recovery && !root.pebble.upgradingFirmware
                color: LomiriColors.orange
                Layout.alignment: Qt.AlignCenter
            }

            Label {
                text: i18n.tr("Upgrading...")
                fontSize: "large"
                Layout.alignment: Qt.AlignCenter
                visible: upgradeIcon.visible
            }
            Rectangle {
                id: upgradeIcon
                height: units.gu(10)
                width: height
                radius: width / 2
                color: LomiriColors.orange
                Layout.alignment: Qt.AlignCenter
                Icon {
                    anchors.fill: parent
                    anchors.margins: units.gu(1)
                    name: "preferences-system-updates-symbolic"
                    color: "white"
                }

                RotationAnimation on rotation {
                    duration: 2000
                    loops: Animation.Infinite
                    from: 0
                    to: 360
                    running: upgradeIcon.visible
                }
                visible: root.pebble.connected && root.pebble.upgradingFirmware
            }
        }
    }

    function calculateOverviewSteps() {
        var dateStart = new Date();
        var dateEnd = new Date();

        dateStart.setHours(0);
        dateStart.setMinutes(0);
        dateStart.setSeconds(0);

        var steps = root.pebble.stepsDataForDay(dateStart, dateEnd).totalSteps;
        
        if (steps === 0)
            return i18n.tr("No steps today");

        return i18n.tr("%1 steps today").arg(steps);
    }

    function calculateOverviewSleep() {
        var dateStart = new Date();
        var dateEnd = new Date();

        dateStart.setHours(0);
        dateStart.setMinutes(0);
        dateStart.setSeconds(0);

        var sleepData = root.pebble.sleepDataForDay(dateStart, dateEnd);
        var sleep = 0;

        for (let i = 0; i < sleepData.length; i++) {
            var map = sleepData[i];
            if (map.type === 1) // normal sleep duration
                sleep += map.duration;
        }

        // Backend returns duration in seconds
        if (sleep >= 3600) {
            let hours = Math.floor(sleep / 3600.0);
            let minutes = Math.floor((sleep % 3600) / 60);

            let retString = "";
            let hourString = i18n.tr("Slept %1 hour", "Slept %1 hours", hours).arg(hours);
            let minuteString = i18n.tr(", %1 minute", ", %1 minutes", minutes).arg(minutes);

            retString += hourString;
            if (minutes > 0)
                retString += minuteString;

            return retString += i18n.tr(" today");
        } else if (sleep < 3600 && sleep > 0) {
            sleep /= 60;
            return i18n.tr("Slept %1 minute today", "Slept %1 minutes today", sleep).arg(sleep);
        }

        return i18n.tr("No sleep today");
    }
}