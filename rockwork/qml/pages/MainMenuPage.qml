import QtQuick 2.4
import QtQuick.Controls 2.4 as QQC2
import QtQuick.Layouts 1.12
import Lomiri.Components 1.3
import QtQml.Models 2.12

Page {
    id: root
    property var pebble: null

    header: PageHeader {
        title: stack.currentItem.title
        trailingActionBar.actions: stack.currentItem.actions
    }

    anchors.fill: parent

    StackLayout {
        id: stack
        property var pebble: root.pebble // for PageLoader to access
        property var pageStack: root.pageStack

        property var currentItem: {
            return stack.children[currentIndex];
        }

        anchors {
            left: parent.left
            right: parent.right
            top: header.bottom
            bottom: bar.top
        }
        anchors.topMargin: units.gu(1)

        currentIndex: bar.currentIndex

        onCurrentIndexChanged: {
            /* HACK: needed until we migrate to Qt 5.15 and every child has the `index`
             * property attached to it
             * Let the current page know it got focused
            */

            stack.children[currentIndex].focused = true;

            for (let i = 0; i < stack.children.length; i++) {
                if (i != currentIndex) {
                    stack.children[i].focused = false;
                }
            }
        }

        PageLoader {
            visible: root.pebble.healthSupported
            page: "HealthPage.qml"
        }
        PageLoader {
            page: "InstalledAppsPage.qml"
            options: { "showWatchFaces": true }
        }
        PageLoader {
            focused: true
            page: "OverviewPage.qml"
        }
        PageLoader {
            page: "InstalledAppsPage.qml"
            options: { "showWatchApps": true }
        }
        PageLoader {
            page: "NotificationsPage.qml"
        }
        PageLoader {
            page: "SettingsPage.qml"
        }
    }

    QQC2.TabBar {
        id: bar
        anchors.bottom: parent.bottom
        width: parent.width
        visible: !root.pebble.upgradingFirmware && !root.pebble.recovery

        currentIndex: 2

        NavigationButton {
            visible: root.pebble.healthSupported
            width: visible ? undefined : 0
            icon.name: "like"
        }

        NavigationButton {
            icon.name: "clock-app-symbolic"
        }

        NavigationButton {
            icon.name: "smartwatch-symbolic"
        }

        NavigationButton {
            icon.name: "stock_application"
        }

        NavigationButton {
            icon.name: "notification"
        }

        NavigationButton {
            icon.name: "settings"
        }
    }

    // For minimizing the amount of time the UI is stuck
    ActivityIndicator {
        anchors.centerIn: parent
        running: stack.currentItem.status !== Loader.Ready
    }

    Connections {
        target: pebble
        onOpenURL: {
            if (url) {
                pageStack.push(Qt.resolvedUrl("AppSettingsPage.qml"), {uuid: uuid, url: url, pebble: pebble})
            }
        }
    }
}
