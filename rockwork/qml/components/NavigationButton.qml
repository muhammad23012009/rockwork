import QtQuick 2.4
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.1

TabButton {
    icon.height: units.gu(3)
    icon.width: units.gu(3)
}
