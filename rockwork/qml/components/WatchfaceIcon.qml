import QtQuick 2.4
import Lomiri.Components 1.3

Item {
    id: root

    property string hardwarePlatform: ""
    property string activeWatchface: ""
    property bool focused: false

    property string iconSource: ""
    property string uuid: ""

    property string watchfacePath: "qrc:///artwork/watchfaces/"

    LomiriShape {
        color: "transparent"
        radius: units.gu(2.5)
        anchors.fill: parent

        source: AnimatedImage {
            id: icon

            anchors.fill: parent
            fillMode: Image.PreserveAspectCrop
            source: {
                switch (root.uuid) {
                case "{8f3c8686-31a1-4f5f-91f5-01600c9bdc59}":
                    if (root.hardwarePlatform === "chalk") {
                        // use accurate watchface according to the size
                    }

                    return watchfacePath + root.hardwarePlatform + "/tictoc.png";

                case "{3af858c3-16cb-4561-91e7-f1ad2df8725f}":
                    return watchfacePath + root.hardwarePlatform + "/kickstart.png";
                }
                return "file://" + root.iconSource
            }
            asynchronous: true

            playing: root.focused && root.activeWatchface === root.uuid
            onPlayingChanged: {
                if (!playing && root.activeWatchface !== root.uuid) {
                    icon.currentFrame = 0;
                }
            }
        }
    }
}