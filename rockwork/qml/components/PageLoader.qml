import QtQuick 2.4
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.12
import Lomiri.Components 1.3

Loader {
    id: loader
    property string page
    property variant options
    property bool focused: false

    property string title: ""
    property list<Action> actions

    Binding {
        target: item
        property: "focused"
        value: loader.focused
    }

    Binding {
        target: loader
        property: "title"
        value: item.title
        when: loader.focused
    }
    Binding {
        target: loader
        property: "actions"
        value: item.actions
        when: loader.focused
    }
    asynchronous: true
    opacity: status === Loader.Ready && focused ? 1 : 0

    // Animates opacity change
    Behavior on opacity {
        NumberAnimation { easing.type: Easing.InQuad; duration: 100 }
    }

    Component.onCompleted: {
        var opt = {};
        opt["pebble"] = parent.pebble;
        opt["pageStack"] = parent.pageStack;
        Object.assign(opt, loader.options);
        loader.setSource(Qt.resolvedUrl(loader.page), opt);
    }
}