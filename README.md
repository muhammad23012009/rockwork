# Rockwork, the Unofficial Ubuntu Touch Pebble Client

An unofficial Pebble App for Ubuntu Touch

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/rockwork.mzanetti)

## Development

* Install `clickable` using your package manager, or `pip`, `pip install clickable-ut`.
* Build RockWork by simply invoking `clickable` to build for your currently connected device.

## i18n: Translating RockWork into your Language

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
Hosted Weblate service:
https://hosted.weblate.org/projects/ubports/rockwork

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.


