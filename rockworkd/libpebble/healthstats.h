#ifndef HEALTHSTATS_H
#define HEALTHSTATS_H

#include "watchconnection.h"
#include "watchdatawriter.h"

#include <array>

class BlobDB;

class HealthStats
{
public:
    enum Day {
        None,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday
    };

    enum ItemKey {
        KeyMondayMovementData,
        KeyTuesdayMovementData,
        KeyWednesdayMovementData,
        KeyThursdayMovementData,
        KeyFridayMovementData,
        KeySaturdayMovementData,
        KeySundayMovementData,

        KeyAverageMonthlySteps,
        KeyAverageMonthlySleep
    };

    HealthStats(BlobDB *blobDb);

    void clearHealthStats();

    void clearMovementData();

    void addMovementData(ItemKey day,
                         int timestamp = 0,
                         int steps = 0,
                         int activeCalories = 0,
                         int restingCalories = 0,
                         int distance_cm = 0,
                         int activeSeconds = 0);

    void setAverageMonthlySteps(int steps);
    void setAverageMonthlySleep(int seconds);

private:
    // Forgive me in advance for what you're about to see...
    QHash<ItemKey, QString> m_keys = {
        { KeyAverageMonthlySteps, "average_dailySteps" },
        { KeyAverageMonthlySleep, "average_sleepDuration" },

        { KeyMondayMovementData, "monday_movementData" },
        { KeyTuesdayMovementData, "tuesday_movementData" },
        { KeyWednesdayMovementData, "wednesday_movementData" },
        { KeyThursdayMovementData, "thursday_movementData" },
        { KeyFridayMovementData, "friday_movementData" },
        { KeySaturdayMovementData, "saturday_movementData" },
        { KeySundayMovementData, "sunday_movementData" }
    };

    QByteArray keyToValue(ItemKey key) {
        return m_keys.value(key).toUtf8();
    }

    BlobDB *m_blobDb;
};

#endif // HEALTHSTATS_H