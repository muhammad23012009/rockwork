#ifndef WEBSERVICES_H
#define WEBSERVICES_H

#include <QObject>
#include <QSettings>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrlQuery>

class Pebble;

class WebServices : public QObject
{
    Q_OBJECT
public:
    WebServices(Pebble *pebble);

    QNetworkRequest authedRequest(const QString &url, const QString &token = QString());

    void setupWebServices(QString stage2);

    QString oauthToken();
    QString bootLink();
    bool loggedIn();
    bool syncFromCloud();
    void setSyncFromCloud(bool enabled);

    QString timelineSyncEndpoint();
    int timelineSyncInterval();
    QString timelineSubscriptionsListEndpoint();
    QString timelineSubscriptionEndpoint();
    QString sandboxUserToken();

    QString accountName();
    QString accountEmail();
    QString accountId();

    QString authenticationEndpoint();
    QString languagePackEndpoint();

    QString lockerAddEndpoint();
    QString lockerRemoveEndpoint();
    QString lockerGetEndpoint();

    bool voiceAllowed();
    bool voiceDictationEnabled();
    void setVoiceDictationEnabled(bool enabled);

    QStringList voiceDictationLanguages();
    QString voiceDictationLanguage();
    void setVoiceDictationLanguage(QString language);
    QString voiceDictationEndpoint();

signals:
    void webServicesUpdated();
    void accountInfoChanged();
    void syncFromCloudChanged(bool enabled);

public slots:
    void changeSyncUrl(QString url);

private:
    Pebble *m_pebble;
    QSettings *m_ini;
    QNetworkAccessManager *m_nam;
    QString m_webservicesPath;
    int m_timerId;

    QVariant iniValue(QString property, QString group = "")
    {
        if (group == m_ini->group() || group.isEmpty())
            return m_ini->value(property);
        else if (!group.isEmpty())
            return m_ini->value(group + "/" + property);

        return QVariant();
    }

    void setIniValue(QString property, QVariant value, QString group = "")
    {
        // Check if we have value set already
        if (iniValue(property, group) == value) return;

        if (group == m_ini->group() || group.isEmpty())
            m_ini->setValue(property, value);
        else if (!group.isEmpty())
            m_ini->setValue(group + "/" + property, value);
    }
};



#endif // WEBSERVICES_H
