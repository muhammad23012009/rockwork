#ifndef APPDOWNLOADER_H
#define APPDOWNLOADER_H

#include <QObject>
#include <QMap>

class QNetworkAccessManager;

class AppDownloader : public QObject
{
    Q_OBJECT
public:
    explicit AppDownloader(const QString &storagePath, QObject *parent = 0);

public slots:
    void downloadApp(const QString &id, bool downloadOnly);

signals:
    void downloadFinished(const QString &id, bool downloadOnly, bool isSideloaded = false);

private slots:
    void appJsonFetched();
    void packageFetched();
    void iconFetched();

private:
    void fetchPackage(const QString &url, const QString &storeId);
    void fetchIcon(const QVariantMap &map);

    QNetworkAccessManager *m_nam;
    QString m_storagePath;
    bool m_downloadOnly;
};

#endif // APPDOWNLOADER_H
