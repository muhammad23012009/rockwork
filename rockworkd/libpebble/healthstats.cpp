#include "healthstats.h"
#include "watchdatawriter.h"
#include "blobdb.h"

#include <QDateTime>

HealthStats::HealthStats(BlobDB *blobDb):
  m_blobDb(blobDb)
{
}

void HealthStats::clearHealthStats()
{
    m_blobDb->clear(BlobDB::BlobDBIdHealthStats);
}

void HealthStats::clearMovementData()
{
    QDateTime today = QDateTime(QDate::currentDate());
    QDateTime endDate = today.addDays(-6);

    for (auto i = today, end = endDate; i >= end; i = i.addDays(-1)) {
        addMovementData((ItemKey) (i.date().dayOfWeek() - 1));
    }
}

void HealthStats::addMovementData(ItemKey day, int timestamp, int steps, int activeCalories, int restingCalories, int distance_cm, int activeSeconds)
{
    QByteArray ret;
    WatchDataWriter writer(&ret);
    writer.writeLE<quint32>(1); // Movement data version
    writer.writeLE<quint32>((!timestamp ? QDateTime::currentDateTime().toSecsSinceEpoch() : timestamp));
    writer.writeLE<quint32>(steps);

    // Watch expects calories in kilocals
    writer.writeLE<quint32>(activeCalories / 1000);
    writer.writeLE<quint32>(restingCalories / 1000);

    // Watch seems to expect data in km?
    writer.writeLE<quint32>(distance_cm / 100000);
    writer.writeLE<quint32>(activeSeconds);

    m_blobDb->insert(BlobDB::BlobDBIdHealthStats, keyToValue(day), ret);
}

void HealthStats::setAverageMonthlySteps(int steps)
{
    QByteArray ret;
    WatchDataWriter writer(&ret);
    writer.writeLE<quint32>(steps);

    m_blobDb->insert(BlobDB::BlobDBIdHealthStats, keyToValue(KeyAverageMonthlySteps), ret);
}

void HealthStats::setAverageMonthlySleep(int seconds)
{
    QByteArray ret;
    WatchDataWriter writer(&ret);
    writer.writeLE<quint32>(seconds);

    m_blobDb->insert(BlobDB::BlobDBIdHealthStats, keyToValue(KeyAverageMonthlySleep), ret);
}