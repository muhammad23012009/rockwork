#include "pingpong.h"

#include "pebble.h"
#include "watchconnection.h"
#include "watchdatareader.h"
#include "watchdatawriter.h"

PingPong::PingPong(Pebble *pebble, WatchConnection *connection):
  QObject(pebble),
  m_pebble(pebble),
  m_connection(connection),
  m_cookie(0)
{
    m_connection->registerEndpointHandler(WatchConnection::EndpointPingPong, this, "handlePingPong");
}

void PingPong::sendPing()
{
    QByteArray data;
    WatchDataWriter writer(&data);

    m_cookie = rand() % 0xFFFFFFFF;

    writer.write<quint8>(PING_PING);
    writer.write<quint32>(m_cookie);
    m_connection->writeToPebble(WatchConnection::EndpointPingPong, data);
}

void PingPong::handlePingPong(const QByteArray &data)
{
    WatchDataReader reader(data);
    PingPongCommand command = (PingPongCommand)reader.read<quint8>();
    quint32 cookie = reader.read<quint32>();

    if (command == PING_PING) {
        QByteArray reply;
        WatchDataWriter writer(&reply);
        writer.write<quint8>(PING_PONG);
        writer.write<quint32>(cookie);
        m_connection->writeToPebble(WatchConnection::EndpointPingPong, reply);
    } else if (command == PING_PONG) {
        if (cookie != m_cookie) {
            qWarning() << "Invalid cookie received. Expected" << m_cookie << "got" << cookie;
        }
    }
}
