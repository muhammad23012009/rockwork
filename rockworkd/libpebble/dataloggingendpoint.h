#ifndef DATALOGGINGENDPOINT_H
#define DATALOGGINGENDPOINT_H

#include <QObject>
#include <QUuid>
#include <QMap>

class Pebble;
class WatchConnection;

struct StepsRecord;
struct OverlayRecord;
struct BatteryRecord;
enum OverlayType : quint8;

enum SessionItemType {
    ByteArrayData = 0x00,
    UnsignedIntData = 0x02,
    SignedIntData = 0x03
};

enum DataLoggingSessionType {
    DataLoggingSessionNormal,
    DataLoggingSessionAnalytics,
    DataLoggingSessionHealthSteps,
    DataLoggingSessionHealthSleep,
    DataLoggingSessionHealthOverlayData,
    DataLoggingSessionHealthHR
};

struct DataLoggingSession {
    QUuid appUuid;
    quint32 timestamp;
    DataLoggingSessionType type;
    SessionItemType itemType;
    quint16 itemSize;
};

class DataLoggingEndpoint : public QObject
{
    Q_OBJECT
public:
    enum DataLoggingCommand {
        DataLoggingDespoolOpenSession = 0x01,
        DataLoggingDespoolSendData = 0x02,
        DataLoggingCloseSession = 0x03,
        DataLoggingReportOpenSessions = 0x84,
        DataLoggingACK = 0x85,
        DataLoggingNACK = 0x86,
        DataLoggingTimeout = 0x07,
        DataLoggingEmptySession = 0x88,
        DataLoggingGetSendEnableRequest = 0x89,
        DataLoggingGetSendEnableResponse = 0x0A,
        DataLoggingSetSendEnable = 0x8B
    };

    enum RecordVersion {
        VERSION_FW_3_10_AND_BELOW = 5,
        VERSION_FW_3_11 = 6,
        VERSION_FW_4_0 = 7,
        VERSION_FW_4_1 = 8,
        VERSION_FW_4_3 = 13
    };

    explicit DataLoggingEndpoint(Pebble *pebble, WatchConnection *connection);

    QMap<quint8, DataLoggingSession> sessionsList();
    void getFullSession(quint8 sessionId);

    void sendACK(quint8 sessionId);

signals:
    void stepsDataChanged(QList<StepsRecord> &record);
    void overlayDataChanged(QList<OverlayRecord> &record);
    void batteryStatisticsChanged(BatteryRecord &record);

private slots:
    void handleMessage(const QByteArray &data);

private:
    QMap<quint8, DataLoggingSession> m_loggingSessions;
    Pebble *m_pebble;
    WatchConnection *m_connection;
};

#endif // DATALOGGINGENDPOINT_H
