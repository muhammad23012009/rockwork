#include "jskitdocument.h"
#include "jskitcanvas/context2d.h"

JSKitDocument::JSKitDocument(QJSEngine *engine) :
  QObject(engine),
  m_engine(0)
{
    m_engine = engine;
}

JSKitDocument::~JSKitDocument()
{
}

QJSValue JSKitDocument::createElement(QString name)
{
    if (name == "canvas") {
        Context2DWrapper *wrapper = new Context2DWrapper(m_engine);
        return m_engine->newQObject(wrapper);
    }
}