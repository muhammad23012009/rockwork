#ifndef JSKIT_DOCUMENT_H
#define JSKIT_DOCUMENT_H

#include <QObject>
#include <QJSEngine>

class JSKitDocument : public QObject
{
    Q_OBJECT

public:
    JSKitDocument(QJSEngine *engine);
    ~JSKitDocument();

public slots:
    QJSValue createElement(QString name);

private:
    QJSEngine *m_engine;
};

#endif // JSKIT_DOCUMENT_H