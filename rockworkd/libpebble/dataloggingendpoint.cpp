#include "dataloggingendpoint.h"

#include "pebble.h"
#include "watchconnection.h"
#include "watchdatareader.h"
#include "watchdatawriter.h"
#include "healthdata.h"
#include "batterylevel.h"

DataLoggingEndpoint::DataLoggingEndpoint(Pebble *pebble, WatchConnection *connection):
    QObject(pebble),
    m_pebble(pebble),
    m_connection(connection)
{
    m_connection->registerEndpointHandler(WatchConnection::EndpointDataLogging, this, "handleMessage");

    QObject::connect(m_connection, &WatchConnection::watchConnected, this, [&]() {
        QByteArray reply;
        WatchDataWriter writer(&reply);
        writer.write<quint8>(DataLoggingReportOpenSessions);
        m_connection->writeToPebble(WatchConnection::EndpointDataLogging, reply);
    });
}

QMap<quint8, DataLoggingSession> DataLoggingEndpoint::sessionsList()
{
    return m_loggingSessions;
}
// This seems to not work, no idea why
void DataLoggingEndpoint::getFullSession(quint8 sessionId)
{
    qWarning() << "getting full session";
    QByteArray data;
    WatchDataWriter writer(&data);

    writer.write<quint8>(DataLoggingEmptySession);
    writer.write<quint8>(sessionId);

    m_connection->writeToPebble(WatchConnection::EndpointDataLogging, data);
}

void DataLoggingEndpoint::handleMessage(const QByteArray &data)
{
    qDebug() << "data logged" << data.toHex();
    WatchDataReader reader(data);
    DataLoggingCommand command = (DataLoggingCommand)reader.read<quint8>();
    quint8 sessionId = reader.read<quint8>();

    switch (command) {
    case DataLoggingDespoolOpenSession: {
        DataLoggingSession session;
        session.appUuid = reader.readUuid();
        session.timestamp = reader.readLE<quint32>();
        quint32 logTag = reader.readLE<quint32>();
        session.itemType = (SessionItemType)reader.read<quint8>();
        session.itemSize = reader.readLE<quint16>();

        if (!m_loggingSessions.contains(sessionId)) {
            if (logTag == 78) {
                session.type = DataLoggingSessionAnalytics;
            } else if (logTag == 81) {
                session.type = DataLoggingSessionHealthSteps;
            } else if (logTag == 83) {
                session.type = DataLoggingSessionHealthSleep;
            } else if (logTag == 84) {
                session.type = DataLoggingSessionHealthOverlayData;
            } else if (logTag == 85) {
                session.type = DataLoggingSessionHealthHR;
            } else {
                session.type = DataLoggingSessionNormal;
            }

            m_loggingSessions.insert(sessionId, session);
        }
        break;
    }

    case DataLoggingDespoolSendData: {
        quint32 itemsLeft = reader.readLE<quint32>();
        quint32 crc = reader.readLE<quint32>();
        quint32 length = data.length() - 10;
        qWarning() << "Despooling data: Session:" << sessionId << "Items left:" << itemsLeft << "CRC:" << crc << "Bytes:" << data.length() << m_loggingSessions.value(sessionId).type;

        if (m_loggingSessions.contains(sessionId)) {
            DataLoggingSession loggingSession = m_loggingSessions.value(sessionId);

            if (loggingSession.type == DataLoggingSessionAnalytics) {
                reader.skip(3);
                quint32 messageTs = reader.readLE<quint32>();

                reader.skip(12);
                quint16 mvolts = reader.readLE<quint16>();

                reader.skip(2);
                quint8 percentage = reader.read<quint8>();

                BatteryRecord record;
                record.percentage = percentage;
                record.millivolts = mvolts;
                record.messageTs = messageTs;
                emit batteryStatisticsChanged(record);

            } else if (loggingSession.type == DataLoggingSessionHealthSteps) {
                quint32 timestamp;
                quint8 recordLength, recordNum;
                RecordVersion recordVersion;

                int packetCount = length / loggingSession.itemSize;

                if ((length % loggingSession.itemSize) != 0) {
                    qWarning() << "malformed data";
                    return;
                }

                for (int i = 0; i < packetCount; i++) {
                    recordVersion = (RecordVersion)reader.readLE<quint16>();
                    if ((recordVersion != VERSION_FW_3_10_AND_BELOW) &&
                        (recordVersion != VERSION_FW_3_11) &&
                        (recordVersion != VERSION_FW_4_0) &&
                        (recordVersion != VERSION_FW_4_1) &&
                        (recordVersion != VERSION_FW_4_3)) {
                        return;
                    }

                    timestamp = reader.readLE<quint32>();
                    reader.read<quint8>(); // skip this
                    recordLength = reader.read<quint8>();
                    recordNum = reader.read<quint8>();

                    QList<StepsRecord> stepsRecords;
                    for (int j = 0; j < recordNum; j++) {
                        StepsRecord record;
                        record.timestamp = timestamp;
                        record.version = recordVersion;

                        record.steps = reader.read<quint8>();
                        record.orientation = reader.read<quint8>();
                        record.intensity = reader.readLE<quint16>();
                        record.light_intensity = reader.read<quint8>();

                        if (recordVersion >= VERSION_FW_3_10_AND_BELOW)
                            record.flags = reader.read<quint8>();

                        if (recordVersion >= VERSION_FW_3_11) {
                            record.resting_gram_calories = reader.readLE<quint16>();
                            record.active_gram_calories = reader.readLE<quint16>();
                            record.distance_cm = reader.readLE<quint16>();
                        }

                        if (recordVersion >= VERSION_FW_4_0)
                            record.heart_rate = reader.read<quint8>();

                        // figure out what to do with these
                        if (recordVersion >= VERSION_FW_4_1)
                            quint16 heartRateWeight = reader.readLE<quint16>();

                        if (recordVersion >= VERSION_FW_4_3)
                            quint8 heartRateZone = reader.read<quint8>();

                        stepsRecords.append(record);
                        timestamp += 60;
                    }
                    emit stepsDataChanged(stepsRecords);
                }

            } else if (loggingSession.type == DataLoggingSessionHealthSleep) {
                int recordCount = length / loggingSession.itemSize;

                if ((length % loggingSession.itemSize) != 0)
                    return;
                
                for (int i = 0; i < recordCount; i++) {
                    QByteArray data = reader.readBytes(loggingSession.itemSize);
                    WatchDataReader sleepDataReader(data);

                    quint16 recordVersion = sleepDataReader.readLE<quint16>();
                    int offsetUTC = sleepDataReader.readLE<quint32>();
                    int bedTimeStart = sleepDataReader.readLE<quint32>();
                    int bedTimeEnd = sleepDataReader.readLE<quint32>();
                    int deepSleepSeconds = sleepDataReader.readLE<quint32>();

                    qWarning() << "have sleep record" << recordVersion << offsetUTC << bedTimeStart << bedTimeEnd << deepSleepSeconds;
                }

            } else if (loggingSession.type == DataLoggingSessionHealthOverlayData) {
                if ((length % loggingSession.itemSize) != 0)
                    return;
                
                int recordCount = length / loggingSession.itemSize;

                QList<OverlayRecord> overlayRecords;
                for (int i = 0; i < recordCount; i++) {
                    OverlayRecord record;
                    record.version = reader.readLE<quint16>();
                    reader.readLE<quint16>(); // skip this
                    record.type = (OverlayType)reader.readLE<quint16>();

                    if (record.type != OverlayTypeSleep &&
                        record.type != OverlayTypeDeepSleep &&
                        record.type != OverlayTypeNap &&
                        record.type != OverlayTypeDeepNap &&
                        record.type != OverlayTypeWalk &&
                        record.type != OverlayTypeRun) {

                        // Skip this data packet
                        reader.skip(loggingSession.itemSize - 6); // We already read 6 bytes, so exclude them
                        continue;
                    }

                    record.offsetUTC = reader.readLE<quint32>();
                    record.startTime = reader.readLE<quint32>();
                    record.duration = reader.readLE<quint32>();

                    if (record.version < 3 || (record.type != OverlayTypeWalk && record.type != OverlayTypeRun)) {
                        record.steps = 0;
                        record.resting_kilo_calories = 0;
                        record.active_kilo_calories = 0;
                        record.distance_cm = 0;
                        if (record.version == 3)
                            reader.skip(8); // As the above data totals out to 8 bytes
                    } else {
                        record.steps = reader.readLE<quint16>();
                        record.resting_kilo_calories = reader.readLE<quint16>();
                        record.active_kilo_calories = reader.readLE<quint16>();
                        record.distance_cm = reader.readLE<quint16>();
                    }
                    overlayRecords.append(record);
                }
                emit overlayDataChanged(overlayRecords);
            }
        }

        break;
    }
    case DataLoggingCloseSession: {
        if (m_loggingSessions.contains(sessionId))
            m_loggingSessions.remove(sessionId);

        qWarning() << "Closing data logging session.";
        break;
    }
    case DataLoggingTimeout: {
        qDebug() << "DataLogging reached timeout: Session:" << sessionId;
        return;
    }
    default:
        qWarning() << "Unhandled DataLogging message" << command;
        return;
    }

    sendACK(sessionId);
}

void DataLoggingEndpoint::sendACK(quint8 sessionId)
{
    QByteArray reply;
    WatchDataWriter writer(&reply);
    writer.write<quint8>(DataLoggingACK);
    writer.write<quint8>(sessionId);
    m_connection->writeToPebble(WatchConnection::EndpointDataLogging, reply);
}