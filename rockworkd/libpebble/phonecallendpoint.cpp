#include "phonecallendpoint.h"

#include "pebble.h"
#include "watchconnection.h"
#include "watchdatareader.h"
#include "watchdatawriter.h"

PhoneCallEndpoint::PhoneCallEndpoint(Pebble *pebble, WatchConnection *connection):
    QObject(pebble),
    m_pebble(pebble),
    m_connection(connection)
{
    m_connection->registerEndpointHandler(WatchConnection::EndpointPhoneControl, this, "handlePhoneEvent");
}

void PhoneCallEndpoint::incomingCall(uint cookie, const QString &number, const QString &name)
{
    QStringList tmp;
    tmp.append(number);
    tmp.append(name);

    char act = CallActionIncoming;

    phoneControl(act, cookie, tmp);

}

void PhoneCallEndpoint::outgoingCall(uint cookie, const QString &number, const QString &name)
{
    QStringList tmp;
    tmp.append(number);
    tmp.append(name);

    char act = CallActionOutgoing;

    phoneControl(act, cookie, tmp);
}

void PhoneCallEndpoint::callStarted(uint cookie)
{
    phoneControl(CallActionStart, cookie, QStringList());
}

void PhoneCallEndpoint::callEnded(uint cookie, bool missed)
{
    Q_UNUSED(missed)
    // FIXME: The watch doesn't seem to react on Missed... So let's always "End" it for now
//    phoneControl(missed ? CallActionMissed : CallActionEnd, cookie, QStringList());
    phoneControl(CallActionEnd, cookie, QStringList());
}

void PhoneCallEndpoint::phoneControl(char act, uint cookie, QStringList datas)
{
    QByteArray data;
    switch (act) {
    case CallActionEnd:
    case CallActionMissed:
    case CallActionIncoming:
        data = serialize(act, cookie, datas);
        break;
    case CallActionOutgoing: {
        // Pebble doesn't correctly deal with outgoing calls, so let's deal with it ourselves.
        QByteArray callmsg, startmsg, data;
        callmsg = serialize(CallActionIncoming, cookie, datas);
        startmsg = serialize(CallActionStart, cookie, datas);
        data.append(m_connection->encodeMessage(WatchConnection::EndpointPhoneControl, callmsg));
        data.append(m_connection->encodeMessage(WatchConnection::EndpointPhoneControl, startmsg));
        m_connection->writeRawData(data);
        return;
        break;
    }
    default:
        break;
    }
    m_connection->writeToPebble(WatchConnection::EndpointPhoneControl, data);
}

void PhoneCallEndpoint::handlePhoneEvent(const QByteArray &data)
{

    WatchDataReader reader(data);
    quint8 command = reader.read<quint8>();
    quint32 cookie = reader.read<quint32>();
    QList<CallState> res;
    quint8 len;

    switch(command) {
    case CallActionAnswer:
        emit answerCall(cookie);
        break;
    case CallActionHangup:
        emit hangupCall(cookie);
        break;
    case CallActionResState:
        while(!reader.checkBad()) {
            CallState cs;
            len = reader.read<quint8>();
            if(len != sizeof(CallState)) {
                qWarning() << "Data corruption:" << len << "does not match expected payload size" << sizeof(CallState);
                break;
            }
            cs.action = reader.read<quint8>();
            cs.cookie = reader.read<quint32>();
            res.append(cs);
        }
        emit callState(cookie, res);
        break;
    default:
        qWarning() << "received an unhandled phone event" << data.toHex();
    }
}

QByteArray PhoneCallEndpoint::serialize(char act, uint cookie, QStringList datas)
{
    QByteArray data;
    WatchDataWriter writer(&data);
    writer.write<quint8>(act);
    writer.write<quint32>(cookie);
    if (datas.length() > 1) {
        writer.writePascalString(datas.at(0));
        writer.writePascalString(datas.at(1));
    } else {
        // Fill in dummy values
        writer.writePascalString("unknown");
        writer.writePascalString("unknown");
    }
    return data;
}