#include "healthdata.h"
#include "healthstats.h"
#include "watchdatareader.h"
#include "watchdatawriter.h"
#include "dataloggingendpoint.h"

#include <QTimer>
#include <QSettings>
#include <QDir>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlResult>
#include <QSqlRecord>

HealthData::HealthData(Pebble *pebble, WatchConnection *connection):
    QObject(pebble),
    m_pebble(pebble),
    m_connection(connection)
{
    m_stats = new HealthStats(m_pebble->blobdb());

    m_timer = new QTimer(this);
    m_timer->setInterval(24 * 3600 * 1000);
    connect(m_timer, &QTimer::timeout, this, &HealthData::setHealthStats);

    QString dbPath = pebble->storagePath();
    m_settings = new QSettings(dbPath + "/healthsettings.conf", QSettings::IniFormat);
    m_db = QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"));
    m_tableModel = new QSqlTableModel(this, m_db);
    QDir dir;
    dir.mkpath(dbPath);
    m_db.setDatabaseName(dbPath + "/healthdata.sqlite");
    initdb();

    connect(m_connection, &WatchConnection::watchConnected, [this]() {
        if (!m_settings->value("healthDataSynced").toBool())
            fetchHealthData(true);
        
        m_timer->start();
    });
}

bool HealthData::isActiveMinute(int flag) const
{
    return (flag & 2) > 0;
}

bool HealthData::isPluggedIn(int flag) const
{
    return (flag & 1) > 0;
}

QVariantMap HealthData::stepsDataForDay(const QDateTime &startTime, const QDateTime &endTime) const
{
    QString queryString = QString("SELECT steps,active_gram_calories,resting_gram_calories,active_minutes,distance_cm,timestamp from healthdata WHERE (timestamp > %1 AND timestamp <= %2)").arg(startTime.toSecsSinceEpoch()).arg(endTime.toSecsSinceEpoch());
    QSqlQuery query;
    bool ok = query.exec(queryString);

    int totalSteps = 0;
    int distance_cm = 0;
    qlonglong active_calories = 0, resting_calories = 0;
    int active_minutes = 0;

    QVariantMap ret;
    QVariantList stepsList;

    while (query.next()) {
        QVariantMap entry;
        entry.insert("steps", query.value(0).toInt());
        entry.insert("timestamp", query.value(5).toLongLong());
        stepsList.append(entry);

        totalSteps += query.value(0).toInt();
        active_calories += query.value(1).toLongLong();
        resting_calories += query.value(2).toLongLong();
        active_minutes += query.value(3).toLongLong();
        distance_cm += query.value(4).toInt();
    }
    ret.insert("steps", stepsList);
    ret.insert("totalSteps", totalSteps);
    ret.insert("active_calories", active_calories);
    ret.insert("resting_calories", resting_calories);
    ret.insert("active_minutes", active_minutes);
    ret.insert("distance_cm", distance_cm);

    return ret;
}

QVariantMap HealthData::stepsDataForWeek(const QDateTime &startTime) const
{
    QDateTime today = QDateTime::currentDateTime();
    QString queryString = QString("SELECT SUM(steps),SUM(active_gram_calories + resting_gram_calories),SUM(active_minutes),SUM(distance_cm) from healthdata WHERE (timestamp > %1 AND timestamp <= %2)");

    QVariantMap ret;
    QVariantList steps;

    int maxSteps = 0;
    int averageSteps = 0;
    qlonglong averageDistance = 0;
    int averageCalories = 0;
    int averageActiveTime = 0;
    int count = 0;

    for (int day = 0; day < 7; day++) {
        QSqlQuery query;
        QVariantMap map;

        QDateTime blockStartTime = startTime.addDays(day);
        QDateTime blockEndTime = blockStartTime.addDays(1);

        if (blockStartTime <= today) {
            query.exec(queryString.arg(blockStartTime.toSecsSinceEpoch()).arg(blockEndTime.toSecsSinceEpoch()));
            while (query.next()) {
                int steps = query.value(0).toInt();
                map.insert("steps", steps);

                averageSteps += steps;
                averageDistance += query.value(3).toLongLong();
                averageCalories += query.value(1).toInt();
                averageActiveTime += query.value(2).toInt();
                count++;

                if (maxSteps < steps)
                    maxSteps = steps;
            }
            steps.append(map);
        } else {
            map.insert("steps", -1);
            steps.append(map);
        }
    }

    ret.insert("steps", steps);
    ret.insert("maxSteps", maxSteps);
    ret.insert("averageSteps", averageSteps / count);
    ret.insert("averageDistance", averageDistance / count);
    ret.insert("averageCalories", averageCalories / count);
    ret.insert("averageActiveTime", averageActiveTime / count);

    return ret;
}

QVariantMap HealthData::averageStepsData(const QDate &startDate, const QDate &endDate) const
{
    QDateTime startTime(startDate);
    QDateTime endTime(endDate);
    QVariantMap data = stepsDataForDay(startTime, endTime);
    QVariantList stepsData = data.value("steps").toList();

    int count = startDate.daysTo(endDate);
    int averageSteps = data.value("totalSteps").toInt();
    int averageActiveTime = 0;
    int averageCalories = 0;
    int averageDistance = 0;

    for (int i = 0; i < stepsData.size(); i++) {
        QVariantMap map = stepsData.value(i).toMap();
        averageActiveTime += map.value("active_minute").toInt();
        averageCalories += map.value("active_calories").toInt() + map.value("resting_calories").toInt();
        averageDistance += map.value("distance_cm").toInt();
    }

    averageSteps /= count;
    averageActiveTime /= count;
    averageCalories /= count;
    averageDistance /= count;

    QVariantMap ret;
    ret.insert("averageSteps", averageSteps);
    ret.insert("averageActiveTime", averageActiveTime);
    ret.insert("averageCalories", averageCalories);
    ret.insert("averageDistance", averageDistance);
    return ret;
}

QVariantList HealthData::overlayDataForDay(const QDateTime &startTime, const QDateTime &endTime, OverlayType type) const
{
    QString queryString = QString("SELECT * from overlaydata WHERE (starttime >= %1 AND starttime <= %2) AND (%3)").arg(startTime.toSecsSinceEpoch()).arg(endTime.toSecsSinceEpoch()).arg(type);

    QSqlQuery query;
    bool result = query.exec(queryString);
    if (!result) {
        qWarning() << "Error executing query:" << query.lastError().text() << "Query was:" << queryString;
    }

    QVariantList ret;
    while (query.next()) {
        QVariantMap entry;
        entry.insert("starttime", query.value(0).toLongLong());
        entry.insert("duration", query.value(1).toInt());
        entry.insert("type", query.value(2).toInt());

        if (type == OverlayTypeWalk || type == OverlayTypeRun) {
            entry.insert("steps", query.value(3).toInt());
            entry.insert("resting_kilo_calories", query.value(4).toInt());
            entry.insert("active_kilo_calories", query.value(5).toInt());
            entry.insert("distance_cm", query.value(6).toInt());
        }
        ret.append(entry);
    }
    return ret;
}

QVariantList HealthData::sleepDataForDay(const QDateTime &startTime, const QDateTime &endTime) const
{
    QString queryString = QString("SELECT starttime,duration,type from overlaydata WHERE (starttime >= %1 AND starttime <= %2) AND (type == 1 OR type == 2)")
                                .arg(startTime.toSecsSinceEpoch()).arg(endTime.toSecsSinceEpoch());
    
    QSqlQuery query;
    bool result = query.exec(queryString);

    QVariantList ret;
    while (query.next()) {
        QVariantMap entry;
        entry.insert("starttime", query.value(0).toLongLong());
        entry.insert("duration", query.value(1).toInt());
        entry.insert("type", query.value(2).toInt());

        ret.append(entry);
    }

    return ret;
}

QVariantMap HealthData::sleepDataForWeek(const QDateTime &startTime) const
{
    qWarning() << "we got called" << startTime;
    QString queryString = QString("SELECT duration,type from overlaydata WHERE (starttime >= %1 AND starttime < %2) AND (type == 1 OR type == 2)");

    int averageDuration = 0;
    int maxDuration = 0;
    int count = 0;

    QVariantMap ret;
    QVariantList sleepList;

    for (int day = 0; day < 7; day++) {
        QSqlQuery query;
        QVariantMap map;

        QDateTime blockStartTime = startTime.addDays(day - 1);
        blockStartTime = blockStartTime.addSecs(3600 * 12);
        QDateTime blockEndTime = blockStartTime.addDays(1);

        if (blockEndTime != QDateTime::currentDateTime().addDays(1)) {
            query.exec(queryString.arg(blockStartTime.toSecsSinceEpoch()).arg(blockEndTime.toSecsSinceEpoch()));

            int sleepDuration = 0, deepSleepDuration = 0;

            while (query.next()) {
                if ((OverlayType)query.value(1).toInt() == OverlayTypeSleep)
                    sleepDuration += query.value(0).toInt();
                else
                    deepSleepDuration += query.value(0).toInt();
            }

            averageDuration += sleepDuration;
            if (maxDuration < sleepDuration)
                maxDuration = sleepDuration;

            map.insert("sleepDuration", sleepDuration);
            map.insert("deepSleepDuration", deepSleepDuration);

            sleepList.append(map);
            count++;
        } else {
            map.insert("sleepDuration", -1);
            map.insert("deepSleepDuration", -1);

            sleepList.append(map);
        }
    }

    ret.insert("sleep", sleepList);
    ret.insert("averageDuration", averageDuration / count);
    ret.insert("maxDuration", maxDuration);

    return ret;
}

int HealthData::sleepAverage(const QDate &startDate, const QDate &endDate, OverlayType type) const
{
    int total = 0;

    QDateTime startTime(startDate);
    QDateTime endTime(endDate);

    QVariantList dayData = sleepDataForDay(startTime, endTime);
    for (int i = 0; i < dayData.count(); i++) {
        if ((OverlayType)dayData.at(i).toMap().value("type").toInt() == type) {
            total += dayData.at(i).toMap().value("duration").toInt();
        }
    }

    return total / startDate.daysTo(endDate);
}

QVariantMap HealthData::averageSleepTimes(const QDate &day) const
{
    bool isWeekend = day.dayOfWeek() >= 6;
    int count = 0;
    quint64 averageFallAsleepTime = 0;
    quint64 averageWakeupTime = 0;
    quint64 averageSleepTime = 0;
    quint64 averageDeepSleep = 0;

    quint64 twentyFour = 24 * 60 * 60 * 1000;
    for (int i = 0; i < 30; i++) {
        if ((isWeekend && day.addDays(-i).dayOfWeek() < 6) || (!isWeekend && day.addDays(-i).dayOfWeek() >= 6)) {
            continue;
        }
        QVariantList dayData = sleepDataForDay(QDateTime(day.addDays(-i)), QDateTime(day.addDays(-i+1)));
        if (dayData.isEmpty()) {
            continue;
        }
        QDateTime fallAsleepTime;
        QDateTime wakeupTime;
        for (int j = 0; j < dayData.count(); j++) {
            QVariantMap entry = dayData.at(j).toMap();
            OverlayType type = (OverlayType)entry.value("type").toInt();
            if (type == OverlayTypeSleep) {
                averageSleepTime += entry.value("duration").toInt();
                QDateTime entryFallAsleepTime = QDateTime::fromMSecsSinceEpoch(entry.value("starttime").toLongLong() * 1000);
                if (fallAsleepTime.isNull() || entryFallAsleepTime < fallAsleepTime) {
                    fallAsleepTime = entryFallAsleepTime;
                }
                QDateTime entryWakeupTime = QDateTime::fromMSecsSinceEpoch((entry.value("starttime").toLongLong() + entry.value("duration").toInt()) * 1000);
                if (wakeupTime.isNull() || entryWakeupTime > wakeupTime) {
                    wakeupTime = entryWakeupTime;
                }
            } else if (type == OverlayTypeDeepSleep) {
                averageDeepSleep += entry.value("duration").toInt();
            }
        }
        count++;
        averageFallAsleepTime += fallAsleepTime.time().msecsSinceStartOfDay();
        if (fallAsleepTime.time() < QTime(12,0,0)) {
            averageFallAsleepTime += twentyFour;
        }
        averageWakeupTime += wakeupTime.time().msecsSinceStartOfDay();
        if (wakeupTime.time() < QTime(18, 0, 0)) {
            averageWakeupTime += twentyFour;
        }

    }
    if (count == 0) {
        qWarning() << "No sleep records available for requested date:" << day;
        return QVariantMap();
    }
    QDateTime sleepDateTime;
    sleepDateTime.setDate(day);
    averageFallAsleepTime = averageFallAsleepTime / count;
    if (averageFallAsleepTime > twentyFour) {
        averageFallAsleepTime -= twentyFour;
        sleepDateTime = sleepDateTime.addDays(-1);
    }
    sleepDateTime.setTime(QTime::fromMSecsSinceStartOfDay(averageFallAsleepTime));

    QDateTime wakeupDateTime;
    wakeupDateTime.setDate(day);
    averageWakeupTime = averageWakeupTime / count;
    if (averageWakeupTime > twentyFour) {
        averageWakeupTime -= twentyFour;
        wakeupDateTime = wakeupDateTime.addDays(-1);
    }
    wakeupDateTime.setTime(QTime::fromMSecsSinceStartOfDay(averageWakeupTime));

    averageSleepTime /= count;
    averageDeepSleep /= count;

    QVariantMap ret;
    ret.insert("fallasleep", sleepDateTime.toMSecsSinceEpoch() / 1000);
    ret.insert("wakeup", wakeupDateTime.toMSecsSinceEpoch() / 1000);
    ret.insert("sleepTime", averageSleepTime);
    ret.insert("deepSleep", averageDeepSleep);
    return ret;
}

#include <QTimeZone>

void HealthData::addHealthData(const QList<StepsRecord> &records)
{
    foreach (StepsRecord record, records) {
        QSqlQuery query;
        QString queryString = QString("INSERT INTO healthdata (timestamp, steps, orientation, intensity, light_intensity, active_minutes, resting_gram_calories, active_gram_calories, distance_cm) VALUES (%1, %2, %3, %4, %5, %6, %7, %8, %9)")
                .arg(record.timestamp)
                .arg(record.steps)
                .arg(record.orientation)
                .arg(record.intensity)
                .arg(record.light_intensity)
                .arg(isActiveMinute(record.flags) ? 1 : 0)
                .arg(record.resting_gram_calories)
                .arg(record.active_gram_calories)
                .arg(record.distance_cm);
        bool result = query.exec(queryString);

        if (!result) {
            qWarning() << "Error inserting health data:" << query.lastError().text();
            qDebug() << "Query was:" << queryString;
        }
    }

    m_settings->setValue("lastSyncTime", QDateTime::currentDateTime().toSecsSinceEpoch());
    emit healthDataChanged();
}

void HealthData::addOverlayData(const QList<OverlayRecord> &records)
{
    foreach (OverlayRecord record, records) {
        QSqlQuery query;
        bool result = query.exec(QString("INSERT INTO overlaydata (starttime, duration, type, steps, resting_kilo_calories, active_kilo_calories, distance_cm) VALUES (%1, %2, %3, %4, %5, %6, %7)")
                            .arg(record.startTime)
                            .arg(record.duration)
                            .arg(record.type)
                            .arg(record.steps)
                            .arg(record.resting_kilo_calories)
                            .arg(record.active_kilo_calories)
                            .arg(record.distance_cm));
        if (!result) {
            qDebug() << "Error inserting overlaydata:" << query.lastError().text();
        }
    }

    m_settings->setValue("lastSyncTime", QDateTime::currentDateTime().toSecsSinceEpoch());
    emit healthDataChanged();
}

void HealthData::initdb()
{
    m_db.open();
    if (!m_db.open()) {
        qWarning() << "Error opening state database:" << m_db.lastError().driverText() << m_db.lastError().databaseText();
        return;
    }

    if (!m_db.tables().contains(QStringLiteral("healthdata"))) {
        QSqlQuery query;
        query.exec(QString("CREATE TABLE healthdata(timestamp INTEGER UNIQUE, steps INTEGER, orientation INTEGER, intensity INTEGER, light_intensity INTEGER, active_minutes INTEGER, resting_gram_calories INTEGER, active_gram_calories INTEGER, distance_cm INTEGER);"));
    }

    // Check if entries exist
    // TODO: remove in v1.17 after users have upgraded to new version and migrated their databases.
    QSqlQuery query;
    bool exists = query.exec("SELECT active_minutes FROM healthdata WHERE 0=1;");
    if (!exists) {
        query.exec("ALTER TABLE healthdata ADD COLUMN active_minutes INTEGER;");
    }
    exists = query.exec("SELECT resting_gram_calories FROM healthdata WHERE 0=1;");
    if (!exists) {
        query.exec("ALTER TABLE healthdata ADD COLUMN resting_gram_calories INTEGER;");
    }
    exists = query.exec("SELECT active_gram_calories FROM healthdata WHERE 0=1;");
    if (!exists) {
        query.exec("ALTER TABLE healthdata ADD COLUMN active_gram_calories INTEGER;");
    }
    exists = query.exec("SELECT distance_cm FROM healthdata WHERE 0=1;");
    if (!exists) {
        query.exec("ALTER TABLE healthdata ADD COLUMN distance_cm INTEGER;");
    }

    if (!m_db.tables().contains(QStringLiteral("overlaydata"))) {
        // We may need to migrate the old sleepdata database over to overlaydata
        // TODO: remove in v1.17
        if (m_db.tables().contains(QStringLiteral("sleepdata"))) {
            // Migrate it over.
            QSqlQuery query;
            bool result = query.exec("ALTER TABLE sleepdata RENAME TO overlaydata;");
            if (!result)
                qWarning() << "Error migrating sleepdata table to overlaydata!" << query.lastError().text();

            // create the new columns
            query.exec("ALTER TABLE overlaydata ADD COLUMN steps INTEGER;");
            query.exec("ALTER TABLE overlaydata ADD COLUMN resting_kilo_calories INTEGER;");
            query.exec("ALTER TABLE overlaydata ADD COLUMN active_kilo_calories INTEGER;");
            query.exec("ALTER TABLE overlaydata ADD COLUMN distance_cm INTEGER;");
        } else {
            QSqlQuery query;
            int result = query.exec(QString("CREATE TABLE overlaydata( \
                                            starttime INTEGER, \
                                            duration INTEGER, \
                                            type INTEGER, \
                                            steps INTEGER, \
                                            resting_kilo_calories INTEGER, \
                                            active_kilo_calories INTEGER, \
                                            distance_cm INTEGER \
                                            );"));
            if (!result) {
                qDebug() << "Error creating overlaydata table:" << query.lastError().text();
            }
        }
    }
}

void HealthData::fetchHealthData(bool firstSync)
{
    QByteArray data;
    WatchDataWriter writer(&data);

    writer.write<quint8>(1);
    if (firstSync) {
        QDateTime currentTime;
        writer.writeLE<quint32>(currentTime.toSecsSinceEpoch());
        m_settings->setValue("healthDataSynced", true);
        m_settings->setValue("lastSyncTime", currentTime.toSecsSinceEpoch());
    } else {
        int timeSinceLastSync = QDateTime::currentDateTime().toSecsSinceEpoch() - m_settings->value("lastSyncTime").toInt();
        if (timeSinceLastSync < 60)
            return;

        writer.writeLE<quint32>(timeSinceLastSync);
        m_settings->setValue("lastSyncTime", QDateTime::currentDateTime().toSecsSinceEpoch());
    }

    m_connection->writeToPebble(WatchConnection::EndpointHealthSync, data);
}

void HealthData::setHealthStats()
{
    QDate startDate = QDate::currentDate();
    QDate endDate = startDate;
    startDate = startDate.addDays(-30);

    int stepCount = averageStepsData(startDate, endDate).value("averageSteps").toInt();
    m_stats->setAverageMonthlySteps(stepCount);

    int sleepDuration = sleepAverage(startDate, endDate, OverlayTypeSleep);
    m_stats->setAverageMonthlySleep(sleepDuration);
}

void HealthData::clearHealthStats()
{
    m_stats->clearHealthStats();
}

void HealthData::setMovementData()
{
    QDateTime today = QDateTime(QDate::currentDate());
    QDateTime endDate = today.addDays(-6);

    for (auto i = today; i >= endDate; i = i.addDays(-1)) {
        QVariantMap data = stepsDataForDay(i, i.addDays(1));
        m_stats->addMovementData((HealthStats::ItemKey) (i.date().dayOfWeek() - 1),
                        i.toSecsSinceEpoch(),
                        data.value("totalSteps").toInt(),
                        data.value("active_calories").toInt(),
                        data.value("resting_calories").toInt(),
                        data.value("distance_cm").toInt(),
                        data.value("active_minutes").toInt());
    }
}