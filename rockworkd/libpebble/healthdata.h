#ifndef HEALTHDATA_H
#define HEALTHDATA_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlTableModel>

#include "pebble.h"

enum OverlayType : quint8 {
    OverlayTypeSleep = 1,
    OverlayTypeDeepSleep = 2,
    OverlayTypeNap = 3,
    OverlayTypeDeepNap = 4,
    OverlayTypeWalk = 5,
    OverlayTypeRun = 6
};
Q_DECLARE_FLAGS(OverlayTypes, OverlayType)

struct StepsRecord {
    quint16 version;
    quint32 timestamp;
    quint8 steps;
    quint8 orientation;
    quint16 intensity;
    quint8 light_intensity;
    quint8 flags;
    quint16 resting_gram_calories;
    quint16 active_gram_calories;
    quint16 distance_cm;
    quint32 heart_rate;
};

struct OverlayRecord {
    quint16 version;
    OverlayType type;
    quint32 offsetUTC;
    quint32 startTime;
    quint32 duration;
    quint16 steps;
    quint16 resting_kilo_calories;
    quint16 active_kilo_calories;
    quint16 distance_cm;
};

class HealthStats;

class HealthData: public QObject
{
    Q_OBJECT
public:

    HealthData(Pebble *pebble, WatchConnection *connection);

    bool isActiveMinute(int flag) const;
    bool isPluggedIn(int flag) const;

    QVariantMap stepsDataForDay(const QDateTime &startTime, const QDateTime &endTime) const;
    QVariantMap stepsDataForWeek(const QDateTime &startTime) const;

    QVariantMap averageStepsData(const QDate &startDate, const QDate &endDate) const;

    QVariantList overlayDataForDay(const QDateTime &startTime, const QDateTime &endTime, OverlayType type) const;

    QVariantList sleepDataForDay(const QDateTime &startTime, const QDateTime &endTime) const;
    QVariantMap sleepDataForWeek(const QDateTime &startTime) const;

    int sleepAverage(const QDate &startDate, const QDate &endDate, OverlayType type) const;
    QVariantMap averageSleepTimes(const QDate &day) const;

    void fetchHealthData(bool firstSync = false);

    void clearHealthStats();
    void setMovementData();

public slots:
    void setHealthStats();

    void addHealthData(const QList<StepsRecord> &records);
    void addOverlayData(const QList<OverlayRecord> &records);

signals:
    void healthDataChanged();

private:
    void initdb();

    Pebble *m_pebble;
    WatchConnection *m_connection;
    HealthStats *m_stats;

    QTimer *m_timer;
    QSettings *m_settings;
    QSqlTableModel *m_tableModel;
    QSqlDatabase m_db;
};

#endif // HEALTHDATA_H
