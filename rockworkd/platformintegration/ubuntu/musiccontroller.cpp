#include "musiccontroller.h"

MusicController::MusicController(QObject *parent):
  QObject(parent),
  manager(new MprisManager(this)),
  m_iface("org.freedesktop.Accounts", "/org/freedesktop/Accounts/User32011", "org.freedesktop.DBus.Properties", QDBusConnection::systemBus())
{
    connect(manager, &MprisManager::currentServiceChanged, this, &MusicController::handleCurrentServiceChanged);
    connect(manager, &MprisManager::playbackStatusChanged, this, &MusicController::handlePlaybackStatusChanged);
    connect(manager, &MprisManager::positionChanged, this, &MusicController::handlePositionChanged); // doesn't seem to work :(
    connect(manager, &MprisManager::metadataChanged, this, &MusicController::handleMetadataChanged);
    connect(manager, &MprisManager::shuffleChanged, this, &MusicController::shuffleChanged);
    connect(manager, &MprisManager::loopStatusChanged, this, &MusicController::repeatChanged);
}

MusicController::~MusicController()
{
}

void MusicController::updateStatus()
{
	QString service = manager->currentService();
	MusicController::Status newStatus;

	if (service.isEmpty()) {
		newStatus =  MusicController::StatusNoPlayer;
	} else {
		switch (manager->playbackStatus()) {
		case Mpris::Playing:
			newStatus = MusicController::StatusPlaying;
			break;
		case Mpris::Paused:
			newStatus = MusicController::StatusPaused;
			break;
		default:
			newStatus = MusicController::StatusStopped;
			break;
		}
	}

	if (newStatus != curStatus) {
		curStatus = newStatus;
		emit statusChanged();
	}
}

void MusicController::updateMetadata()
{
	QVariantMap metadata = manager->metadata();

	QString newArtist = metadata.value("xesam:artist").toString(),
			newAlbum = metadata.value("xesam:album").toString(),
			newTitle = metadata.value("xesam:title").toString();

	if (newArtist != curArtist) {
		curArtist = newArtist;
		emit artistChanged();
	}

	if (newAlbum != curAlbum) {
		curAlbum = newAlbum;
		emit albumChanged();
	}

	if (newTitle != curTitle) {
		curTitle = newTitle;
		emit titleChanged();
	}

	int newDuration = metadata.value("mpris:length").toULongLong() / 1000UL;
	if (newDuration != curDuration) {
		curDuration = newDuration;
		emit durationChanged();
	}

	emit metadataChanged();
}

void MusicController::handleCurrentServiceChanged()
{
    updateStatus();
    emit serviceChanged();
}

void MusicController::handlePlaybackStatusChanged()
{
    updateStatus();
}

void MusicController::handlePositionChanged()
{
    emit positionChanged();
}

void MusicController::handleMetadataChanged()
{
    updateMetadata();
}


MusicController::Status MusicController::status() const
{
	return curStatus;
}

QString MusicController::service() const
{
	return manager->currentService();
}

QVariantMap MusicController::metadata() const
{
	return manager->metadata();
}

QString MusicController::title() const
{
	return curTitle;
}

QString MusicController::album() const
{
	return curAlbum;
}

QString MusicController::artist() const
{
	return curArtist;
}

int MusicController::duration() const
{
	return curDuration;
}

qlonglong MusicController::position() const
{
    return manager->position();
}

MusicController::RepeatStatus MusicController::repeat() const
{
	switch (manager->loopStatus()) {
	case Mpris::None:
	default:
		return RepeatNone;
	case Mpris::Track:
		return RepeatTrack;
	case Mpris::Playlist:
		return RepeatPlaylist;
	}
}

bool MusicController::shuffle() const
{
	return manager->shuffle();
}

void MusicController::play()
{
	manager->play();
}

void MusicController::pause()
{
	manager->pause();
}

void MusicController::playPause()
{
	manager->playPause();
}

void MusicController::next()
{
	manager->next();
}

void MusicController::previous()
{
	manager->previous();
}

void MusicController::volumeUp()
{
    if (m_iface.isValid()) {
        QDBusReply<QDBusVariant> answer = m_iface.call("Get",
                                                       "com.lomiri.AccountsService.Sound",
                                                       "Volume");
        if (answer.isValid()) {
            curVolume = answer.value().variant().toDouble();
            if (curVolume >= 1.0) {
                qDebug() << "Cannot set volume beyond maximum!";
                return;
            }
            newVolume = curVolume + 0.05;

            m_iface.call("Set",
                       "com.lomiri.AccountsService.Sound",
                       "Volume",
                       QVariant::fromValue(QDBusVariant(QVariant::fromValue(newVolume))));
 
        }
    }
}

void MusicController::volumeDown()
{
    if (m_iface.isValid()) {
        QDBusReply<QDBusVariant> answer = m_iface.call("Get",
                                                       "com.lomiri.AccountsService.Sound",
                                                       "Volume");
        if (answer.isValid()) {
            curVolume = answer.value().variant().toDouble();
            if (curVolume == 0) {
                qDebug() << "Cannot decrease volume beyond minimum!";
                return;
            }
            newVolume = curVolume - 0.05;

            m_iface.call("Set",
                       "com.lomiri.AccountsService.Sound",
                       "Volume",
                       QVariant::fromValue(QDBusVariant(QVariant::fromValue(newVolume))));

        }
    }    
}
