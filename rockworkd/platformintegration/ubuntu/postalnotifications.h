#include <QDebug>

#include <QObject>
#include <QDBusInterface>
#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusReply>

#define DBUS_SERVICE "com.lomiri.Postal"
#define DBUS_PATH "/com/lomiri/Postal"
#define DBUS_IFACE "com.lomiri.Postal"

class PostalNotifications : public QObject
{
public:
    PostalNotifications();
    ~PostalNotifications();

public slots:
    void closeNotification(QString appId);

private:
    QDBusConnection m_bus;
};
