#ifndef MUSICCONTROLLER_H
#define MUSICCONTROLLER_H

#include <QtDBus/QDBusInterface>
#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusReply>

#include <QDebug>

#include <MprisQt/mpris.h>
#include <MprisQt/mprismanager.h>

class MusicController : public QObject
{
	Q_OBJECT

public:
	explicit MusicController(QObject *parent = 0);
	~MusicController();

	enum Status {
		StatusNoPlayer = 0,
		StatusStopped,
		StatusPaused,
		StatusPlaying
	};

	enum RepeatStatus {
		RepeatNone = 0,
		RepeatTrack,
		RepeatPlaylist
	};

	Status status() const;
	QString service() const;

	QVariantMap metadata() const;

	QString title() const;
	QString album() const;
	QString artist() const;
        qlonglong position() const;

	int duration() const;

	RepeatStatus repeat() const;
	bool shuffle() const;

public slots:
	void play();
	void pause();
	void playPause();
	void next();
	void previous();

	void volumeUp();
	void volumeDown();

signals:
    void statusChanged();
    void serviceChanged();
    void metadataChanged();
    void positionChanged();
    void titleChanged();
    void albumChanged();
    void artistChanged();
    void durationChanged();
    void repeatChanged();
    void shuffleChanged();

private:
    void updateStatus();
    void updateMetadata();

    MprisManager *manager;
    MusicController::Status curStatus;
    QString curTitle;
    QString curAlbum;
    QString curArtist;
    QString curAlbumArt;
    int curDuration;

    QDBusInterface m_iface;
    double curVolume, newVolume;

private slots:
    void handleCurrentServiceChanged();
    void handlePlaybackStatusChanged();
    void handlePositionChanged();
    void handleMetadataChanged();
};

#endif // MUSICCONTROLLER_H
